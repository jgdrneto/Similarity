package core;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import similarity.core.entities.TreeNode;

public class TreeNodeTest{
	
	TreeNode n1,n2,n3,n4,n5,n6,n7,n8,n9, n10;
	
	@Before
	public void setUp() throws Exception {
		
		n1 = new TreeNode("n1"); 
		n2 = new TreeNode("n2"); 
		n3 = new TreeNode("n3"); 
		n4 = new TreeNode("n4"); 
		n5 = new TreeNode("n5");
		
		n6 = new TreeNode("n1"); 
		n7 = new TreeNode("n2"); 
		n8 = new TreeNode("n3"); 
		n9 = new TreeNode("n4"); 
		n10 = new TreeNode("n5");
	}
	
	@Test
	public void treeDistEqualsTest() {
		
		/* 		
		 *  Create Tree
		 *  
		 * 		n1      
		 * 	   /  \
		 * 	  n2  n3 
		 * 
		 */
		n1.addChildren(n2);
		n1.addChildren(n3);
		
		/* 		
		 *  Create Tree
		 *  
		 * 	    n4      
		 * 	    |  
		 * 	  	n5  
		 * 
		 */
		n4.addChildren(n5);
		
		//Try n1 != n4
		assertEquals(false,n1.equals(n4));
		//Try n4 != n1
		assertEquals(false,n4.equals(n1));
	}
	
	@Test
	public void TreeEquiEqualsTest() {
		
		/* 		
		 *  Create Tree
		 *  
		 * 		n1      
		 * 	   /  \
		 * 	  n2  n3 
		 * 
		 */
		n1.addChildren(n2);
		n1.addChildren(n3);
		
		/* 		
		 *  Create Tree
		 *  
		 * 		n6      
		 * 	   /  \
		 * 	  n7  n8 
		 * 
		 */
		n6.addChildren(n7);
		n6.addChildren(n8);
		
		//Try n1 == n6
		assertEquals(true,n1.equals(n6));
		//Try n6 == n1
		assertEquals(true,n6.equals(n1));
		
	}
	
	@Test
	public void subTreeEqualTest() {
		
		/* 		
		 *  Create Tree
		 *  
		 * 		n1      
		 * 	   /  \
		 * 	  n2  n3 
		 * 
		 */
		n1.addChildren(n2);
		n1.addChildren(n3);
		
		/* 		
		 *  Create Tree
		 *  
		 * 		n6      
		 * 	   /  
		 * 	  n7   
		 * 
		 */
		n6.addChildren(n7);
		
		//Try n1 != n6
		assertEquals(false,n1.equals(n6));
		//Try n6 != n1
		assertEquals(false,n6.equals(n1));
		
	}
	

	@Test
	public void almostEquiTreeEqualsTest() {
		
		/* 		
		 *  Create Tree
		 *  
		 * 		n1      
		 * 	   /  \
		 * 	  n2  n3 
		 * 
		 */
		n1.addChildren(n2);
		n1.addChildren(n3);
		
		/* 		
		 *  Create Tree
		 *  
		 * 		n6      
		 * 	   /  \
		 * 	  n8  n7 
		 * 
		 */
		n6.addChildren(n8);
		n6.addChildren(n7);
		
		//Try n1 == n6
		assertEquals(false,n1.equals(n6));
		//Try n6 == n1
		assertEquals(false,n6.equals(n1));
		
	}
}
