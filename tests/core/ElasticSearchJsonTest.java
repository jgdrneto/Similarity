package core;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import org.junit.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import similarity.elasticSearch.extractor.domain.SinfoErrorLog;
import similarity.elasticSearch.extractor.parser.SinfoErrorLogJsonParser;

public class ElasticSearchJsonTest {
	private ObjectMapper mapper = new ObjectMapper();
	private SinfoErrorLogJsonParser errorParser = new SinfoErrorLogJsonParser();
	private SinfoErrorLogJsonParser errorParserBrUfrn = new SinfoErrorLogJsonParser("br.ufrn\\S+", "\\S+(.get|.set)\\S+");
	
	@Test
	public void sinfoJson1() throws IOException {
		File file = new File("tests/resources/logs/elasticsearch/sinfo1.json");
		JsonNode stackTrace = mapper.readValue(new String(Files.readAllBytes(file.toPath())), JsonNode.class);
		
		SinfoErrorLog log = errorParser.parse(stackTrace);
		assertEquals(134, log.getStackTraceElements().size());
		SinfoErrorLog logBrUfrn = errorParserBrUfrn.parse(stackTrace);
		assertEquals(16, logBrUfrn.getStackTraceElements().size());
	}
	
	@Test
	public void sinfoJson2() throws IOException {
		File file = new File("tests/resources/logs/elasticsearch/sinfo2.json");
		JsonNode stackTrace = mapper.readValue(new String(Files.readAllBytes(file.toPath())), JsonNode.class);
		
		SinfoErrorLog log = errorParser.parse(stackTrace);
		assertEquals(142, log.getStackTraceElements().size());
		SinfoErrorLog logBrUfrn = errorParserBrUfrn.parse(stackTrace);
		assertEquals(15, logBrUfrn.getStackTraceElements().size());
	}
	
	@Test
	public void sinfoJson3() throws IOException {
		File file = new File("tests/resources/logs/elasticsearch/sinfo3.json");
		JsonNode stackTrace = mapper.readValue(new String(Files.readAllBytes(file.toPath())), JsonNode.class);
		
		SinfoErrorLog log = errorParser.parse(stackTrace);
		assertEquals(83, log.getStackTraceElements().size());
		SinfoErrorLog logBrUfrn = errorParserBrUfrn.parse(stackTrace);
		assertEquals(3, logBrUfrn.getStackTraceElements().size());
	}
	
	@Test
	public void simpleJson() throws IOException {
		File file = new File("tests/resources/logs/elasticsearch/simple.json");
		JsonNode stackTrace = mapper.readValue(new String(Files.readAllBytes(file.toPath())), JsonNode.class);
		
		SinfoErrorLog log = errorParser.parse(stackTrace);
		assertTrue(AbstractTraceElementTest.S1.contains(log.getTraceElements().get(0)));
	}
}