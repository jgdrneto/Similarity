package core;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.io.IOException;

import org.junit.Test;

import similarity.elasticSearch.extractor.domain.TraceElementImpl;

public class AbstractTraceElementTest {
	
	public static TraceElementImpl S1;
	public static TraceElementImpl S2;
	public static TraceElementImpl S3;
	public static TraceElementImpl S4;
	public static TraceElementImpl S5;
	
	static {
		//ClassA.method01
		//  ClassA.method02
		//	  ClassA.method03
		//      ClassB.method04
		//        ClassB.method05
		//      ClassC.method06
		//        ClassD.method07
		//        ClassD.method08
		//          ClassE.method09
		//            ClassF.method10
		//              ClassF.method11
		//      ClassB.method04
		//        ClassB.method05
		//          ClassG.method12
		//            ClassG.method13
		//              ClassG.method14
		TraceElementImpl s1_a01 = new TraceElementImpl("ClassA.method01");
		TraceElementImpl s1_a02 = new TraceElementImpl("ClassA.method02");
		TraceElementImpl s1_a03 = new TraceElementImpl("ClassA.method03");
		TraceElementImpl s1_b04 = new TraceElementImpl("ClassB.method04");
		TraceElementImpl s1_b05 = new TraceElementImpl("ClassB.method05");
		TraceElementImpl s1_c06 = new TraceElementImpl("ClassC.method06");
		TraceElementImpl s1_d07 = new TraceElementImpl("ClassD.method07");
		TraceElementImpl s1_d08 = new TraceElementImpl("ClassD.method08");
		TraceElementImpl s1_e09 = new TraceElementImpl("ClassE.method09");
		TraceElementImpl s1_f10 = new TraceElementImpl("ClassF.method10");
		TraceElementImpl s1_f11 = new TraceElementImpl("ClassF.method11");
		TraceElementImpl s1_b04_2 = new TraceElementImpl("ClassB.method04");
		TraceElementImpl s1_b05_2 = new TraceElementImpl("ClassB.method05");
		TraceElementImpl s1_g12 = new TraceElementImpl("ClassG.method12");
		TraceElementImpl s1_g13 = new TraceElementImpl("ClassG.method13");
		TraceElementImpl s1_g14 = new TraceElementImpl("ClassG.method14");
		s1_a01.addChild(s1_a02);
		s1_a02.addChild(s1_a03);
		s1_a03.addChild(s1_b04);
		s1_b04.addChild(s1_b05);
		s1_a03.addChild(s1_c06);
		s1_c06.addChild(s1_d07);
		s1_c06.addChild(s1_d08);
		s1_d08.addChild(s1_e09);
		s1_e09.addChild(s1_f10);
		s1_f10.addChild(s1_f11);
		s1_a03.addChild(s1_b04_2);
		s1_b04_2.addChild(s1_b05_2);
		s1_b05_2.addChild(s1_g12);
		s1_g12.addChild(s1_g13);
		s1_g13.addChild(s1_g14);
		
		S1 = s1_a01;
		
		//ClassA.method03
		//  ClassB.method04
		//    ClassB.method05
		TraceElementImpl s2_a03 = new TraceElementImpl("ClassA.method03");
		TraceElementImpl s2_b04 = new TraceElementImpl("ClassB.method04");
		TraceElementImpl s2_b05 = new TraceElementImpl("ClassB.method05");
		s2_a03.addChild(s2_b04);
		s2_b04.addChild(s2_b05);
		S2 = s2_a03;

		//ClassA.method01
		//  ClassA.method02
		//	  ClassA.method03
		//        ..
		//        ClassB.method05
		//      ..
		//        ClassD.method08
		//          ..
		//            ClassF.method10
		TraceElementImpl s3_a01 = new TraceElementImpl("ClassA.method01");
		TraceElementImpl s3_a02 = new TraceElementImpl("ClassA.method02");
		TraceElementImpl s3_b05 = new TraceElementImpl("ClassB.method05");
		TraceElementImpl s3_a03 = new TraceElementImpl("ClassA.method03");
		TraceElementImpl s3_d08 = new TraceElementImpl("ClassD.method08");
		TraceElementImpl s3_f10 = new TraceElementImpl("ClassF.method10");
		s3_a01.addChild(s3_a02);
		s3_a02.addChild(s3_a03);
		s3_a03.addChild(s3_b05);
		s3_a03.addChild(s3_d08);
		s3_d08.addChild(s3_f10);
		S3 = s3_a01;
		
		//ClassB.method04
		//  ClassB.method05
		//    ClassG.method12
		//      ClassG.method13
		//        ClassG.method14
		TraceElementImpl s4_b04 = new TraceElementImpl("ClassB.method04");
		TraceElementImpl s4_b05 = new TraceElementImpl("ClassB.method05");
		TraceElementImpl s4_g12 = new TraceElementImpl("ClassG.method12");
		TraceElementImpl s4_g13 = new TraceElementImpl("ClassG.method13");
		TraceElementImpl s4_g14 = new TraceElementImpl("ClassG.method14");
		s4_b04.addChild(s4_b05);
		s4_b05.addChild(s4_g12);
		s4_g12.addChild(s4_g13);
		s4_g13.addChild(s4_g14);
		S4 = s4_b04;	
		
		//ClassB.method04
		//  ClassB.method05
		//    ClassB.method06
		TraceElementImpl s5_b04 = new TraceElementImpl("ClassB.method04");
		TraceElementImpl s5_b05 = new TraceElementImpl("ClassB.method05");
		TraceElementImpl s5_b06 = new TraceElementImpl("ClassB.method06");
		s5_b04.addChild(s5_b05);
		s5_b05.addChild(s5_b06);
		S5 = s5_b04;
	}
	
	@Test
	public void qtdDescendents() {
		assertEquals(new Integer(15), S1.getQtdDescendents());
		assertEquals(new Integer(2), S2.getQtdDescendents());
		assertEquals(new Integer(5), S3.getQtdDescendents());
		assertEquals(new Integer(4), S4.getQtdDescendents());
		assertEquals(new Integer(2), S5.getQtdDescendents());
	}
	
	@Test
	public void deveConterSequencia() throws IOException{
		assertTrue(S1.contains(S2));
	}
	
	@Test
	public void deveConterPropriaSequencia() throws IOException{
		assertTrue(S1.contains(S1));
	}
	
	@Test
	public void deveConterSalteado() throws IOException{
		assertTrue(S1.contains(S3));
	}
	
	@Test
	public void deveConterSegundaStackTrace() throws IOException{
		assertTrue(S1.contains(S4));
	}
	
	@Test
	public void naoDeveConterSequencia() throws IOException{
		assertFalse(S1.contains(S5));
	}
}