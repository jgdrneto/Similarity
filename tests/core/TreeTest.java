package core;

import static org.junit.Assert.*;

import java.math.BigInteger;

import org.junit.Before;
import org.junit.Test;

import similarity.core.entities.Tree;
import similarity.core.entities.TreeNode;

public class TreeTest {
	
	Tree t1,t2;
	TreeNode n1,n2,n3,n4,n5,n6,n7,n8,n9, n10;
	
	@Before
	public void setUp() throws Exception {
		
		n1 = new TreeNode("n1"); 
		n2 = new TreeNode("n2"); 
		n3 = new TreeNode("n3"); 
		n4 = new TreeNode("n4"); 
		n5 = new TreeNode("n5");
		
		n6 = new TreeNode("n1"); 
		n7 = new TreeNode("n2"); 
		n8 = new TreeNode("n3"); 
		n9 = new TreeNode("n4"); 
		n10 = new TreeNode("n5");
		
		t1 = new Tree();
		t2 = new Tree();
	}
	
	@Test
	public void distEqualsTest() {
		
		/* 		
		 *  Create Tree (T1)
		 *  
		 * 		n1      
		 * 	   /  \
		 * 	  n2  n3 
		 * 
		 */
		n1.addChildren(n2);
		n1.addChildren(n3);
		t1.setRoot(n1);
		
		/* 		
		 *  Create Tree (T2)
		 *  
		 * 	    n6      
		 * 	    |  
		 * 	  	n7  
		 * 
		 */
		n6.addChildren(n7);
		t2.setRoot(n6);
		
		//Try t1 != t2
		assertEquals(false,t1.equals(t2));
		//Try t2 != t1
		assertEquals(false,t2.equals(t1));
	}
	
	@Test
	public void distNameEqualsTest() {
		
		/* 		
		 *  Create Tree (T1)
		 *  
		 * 		n1      
		 * 	   /  \
		 * 	  n2  n3 
		 * 
		 */
		n1.addChildren(n2);
		n1.addChildren(n3);
		t1.setRoot(n1);
		
		/* 		
		 *  Create Tree (T2)
		 *  
		 * 		n1      
		 * 	   /  \
		 * 	  n2  n3 
		 * 
		 */
		t2.setRoot(n1);
		
		t2.setName("arvore2");
		
		//Try t1 != t2
		assertEquals(false,t1.equals(t2));
		//Try t2 != t1
		assertEquals(false,t2.equals(t1));
	}
	
	@Test
	public void equiEqualsTest() {
		
		/* 		
		 *  Create Tree (T1)
		 *  
		 * 		n1      
		 * 	   /  \
		 * 	  n2  n3 
		 * 
		 */
		n1.addChildren(n2);
		n1.addChildren(n3);
		t1.setRoot(n1);
		
		/* 		
		 *  Create Tree (T2)
		 *  
		 * 		n6      
		 * 	   /  \
		 * 	  n7  n8 
		 * 
		 */
		n6.addChildren(n7);
		n6.addChildren(n8);
		t2.setRoot(n6);
		
		//Try t1 != t2
		assertEquals(true,t1.equals(t2));
		//Try t2 != t1
		assertEquals(true,t2.equals(t1));
	}
	
	@Test
	public void equiHeightTest() {

		/* 		
		 *  Create Tree (T1)
		 *  
		 * 		null       
		 */
		assertEquals(0,t1.getHeight());
		
	}
	
	@Test
	public void equiHeight2Test() {

		/* 		
		 *  Create Tree (T1)
		 *  
		 * 		n1       
		 */
		t1.setRoot(n1);
		
		assertEquals(1,t1.getHeight());
		
	}
	
	@Test
	public void equiHeight3Test() {

		/* 		
		 *  Create Tree (T1)
		 *  
		 * 		n1      
		 * 	   /  \
		 * 	  n2  n3 
		 *    /
		 *   n4 
		 */
		n1.addChildren(n2);
		n1.addChildren(n3);
		n2.addChildren(n4);
		t1.setRoot(n1);
		
		assertEquals(3,t1.getHeight());
		
	}
	
	@Test
	public void equiRealTimeExecutionTest() {

		/* 		
		 *  Create Tree (T1)
		 *  
		 * 		n1      
		 * 	   /  \
		 * 	  n2  n3 
		 *    /
		 *   n4 
		 */
		n1.addChildren(n2);
		n1.addChildren(n3);
		n2.addChildren(n4);
		t1.setRoot(n1);
		
		n1.setRealTime(new BigInteger("100"));
		n2.setRealTime(new BigInteger("50"));
		n3.setRealTime(new BigInteger("50"));
		n4.setRealTime(new BigInteger("200"));
		
		n4.setTime(new BigInteger("200"));
		n3.setTime(new BigInteger("50"));
		n2.setTime(new BigInteger("250"));
		n1.setTime(new BigInteger("400"));
		
		assertEquals(new BigInteger("400"), t1.getTotalTimeExecution());
	}
	
}
