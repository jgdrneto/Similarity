package core;

import static org.junit.Assert.*;

import java.util.List;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.Before;
import org.junit.Test;

import similarity.core.entities.Entity;
import similarity.core.entities.Tree;
import similarity.core.entities.TreeNode;
import similarity.perfMiner.core.PerfMinerExtractor;
import similarity.perfMiner.entity.Scenario;
import similarity.perfMiner.extractor.controllers.ScenarioJPAController;

public class PerfMinerExtractorTest {
	
	public static int quant = 2;
	public static int initRec = 0;
	
	Entity pMManual;
	Entity pMAuto;
	
	@Before
	public void setUp() throws Exception {
		
		PerfMinerExtractor pM_Manual = new PerfMinerExtractor("PerfMiner-ManualTestsPU");
		System.out.println("Manual");
		pMManual = pM_Manual.extractRegister();
		
		
		PerfMinerExtractor pM_Automatico = new PerfMinerExtractor("PerfMiner-AutoTestsPU");
		System.out.println("Automatico");
		pMAuto = pM_Automatico.extractRegister();
		
		
	}

	@Test
	public void checkSizeTreeTest() {
		
		assertEquals(59834,pMManual.getData().size());
		assertEquals(512,pMAuto.getData().size());
		
	}
	@Test
	public void ChildrensTreeTest() {
		
		boolean hasChildrens = false;
				
		for(Tree  t : pMAuto.getData()) {
			
			if(t.getHeight()>1) {				
				
				System.out.println("cenario" + t.getName() + " " + "Altura: " + t.getHeight());
				
				hasChildrens = true;				
						
				break;
			}
		}
		
		assertEquals(true, hasChildrens);
	}
	
}
