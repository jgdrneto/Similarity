package similarity.main;

import java.util.List;

import similarity.core.entities.Entity;
import similarity.core.entities.Execution;
import similarity.core.entities.Tree;
import similarity.core.entities.TreeNode;
import similarity.perfMiner.core.PerfMinerExtractor;
import similarity.perfMiner.entity.Node;
import similarity.perfMiner.extractor.controllers.ExecutionJPAController;
import similarity.perfMiner.extractor.controllers.ExecutionJPAController;
import similarity.perfMiner.extractor.controllers.JPAController;
import similarity.perfMiner.extractor.controllers.NodeJPAController;
import similarity.perfMiner.extractor.controllers.NodeJPAController;
import similarity.perfMiner.extractor.controllers.ScenarioJPAController;

public class Main {

	public static void main(String[] args) {
		/*
		PerfMinerExtractor pM_Manual = new PerfMinerExtractor("PerfMiner-ManualTestsPU");
		System.out.println("Manual");
		Entity pMManual = pM_Manual.extractRegister();

		PerfMinerExtractor pM_Automatico = new PerfMinerExtractor("PerfMiner-AutoTestsPU");
		System.out.println("Automatico");
		Entity pMAuto = pM_Automatico.extractRegister();;
		
	    System.out.println("Quantidade de cenários automáticos: " + pMAuto.getData().size());
        System.out.println("Quantidade de cenários manuais: " + pMManual.getData().size()); 
    	*/
		
		NodeJPAController controller = new NodeJPAController("PerfMiner-AutoTestsPU");
		List<TreeNode> nodes = controller.findEntities();
		
		System.out.println(nodes.size());
		
		for(TreeNode t : nodes) {
			if(t.getChildrens().size()>0) {
				System.out.println(t.getChildrens().get(0).getName());
			}
		}
		
		ExecutionJPAController exec2 = new ExecutionJPAController("PerfMiner-AutoTestsPU");
		
		List<Execution> ex2list = exec2.findEntities();
		
		for(Execution e : ex2list) {
			System.out.println(e.getDate() + " " + e.getSystemName());
		}
		
		ScenarioJPAController scenario2 = new ScenarioJPAController("PerfMiner-AutoTestsPU");
		
		List<Tree> treelist = scenario2.findEntities();
		
		for(Tree t : treelist) {
			
			if(t.getRoot().getChildrens().size()>0) {
				System.out.println(t.getName());
			}
		}
		
		
	}

}
