package similarity.main;

import java.util.ArrayList;
import java.util.List;

import similarity.core.Application;
import similarity.core.Criterion;
import similarity.core.Extractor;
import similarity.core.entities.Pair;
import similarity.criterions.ExecutionTimeCriterion;
import similarity.criterions.MoreExecutionCriterion;
import similarity.perfMiner.core.PerfMinerExtractor;
import similarity.product.moreExecutionTimeAlgorithm.MoreExecutionTimeAlgorithm;

public class Product3 {

	public static void main(String[] args) {
		
		List<Extractor> extractors = new ArrayList<Extractor>(); 
		
		extractors.add(new PerfMinerExtractor("PerfMiner-ManualTestsPU"));
		//extractors.add(new PerfMinerExtractor("PerfMiner-AutoTestsPU"));
		
		List<Pair<Criterion,Integer>> criterions = new ArrayList<Pair<Criterion,Integer>>();
		
		criterions.add(new Pair<Criterion,Integer>(new MoreExecutionCriterion(),30));
		criterions.add(new Pair<Criterion,Integer>(new ExecutionTimeCriterion(),1));
		
		Application c = new MoreExecutionTimeAlgorithm(extractors, criterions);
		
		c.execute();
	}

}
