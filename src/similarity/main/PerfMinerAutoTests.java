/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package similarity.main;

import similarity.core.entities.Tree;
import similarity.perfMiner.entity.Scenario;
import similarity.perfMiner.extractor.controllers.ScenarioJPAController;

import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Jullian
 */
public class PerfMinerAutoTests {
    
    public static void main(String[] args) {
        
    	ScenarioJPAController scenarioAuto = new ScenarioJPAController("PerfMiner-AutoTestsPU");
        ScenarioJPAController scenarioManual = new ScenarioJPAController("PerfMiner-ManualTestsPU");
        
        System.out.println("Manual");
        List<Tree> registrosAuto = scenarioAuto.findEntities();
        System.out.println("Automatico");
        List<Tree> registrosManual = scenarioManual.findEntities();
        
        System.out.println("Quantidade de cenários automáticos: " + registrosAuto.size());
        System.out.println("Quantidade de cenários manuais: " + registrosManual.size()); 
    }
  
}
