package similarity.main;

import java.util.ArrayList;
import java.util.List;

import similarity.core.Application;
import similarity.core.Extractor;
import similarity.perfMiner.core.PerfMinerExtractor;
import similarity.product.similarityAlgorithm.SimilarityAlgorithm;

public class Product1 {

	public static void main(String[] args) {
		
		List<Extractor> extractors = new ArrayList<Extractor>(); 
		
		extractors.add(new PerfMinerExtractor("PerfMiner-ManualTestsPU"));
		extractors.add(new PerfMinerExtractor("PerfMiner-AutoTestsPU"));
		
		Application c = new SimilarityAlgorithm(extractors);
		
		c.execute();
		
	}

}
