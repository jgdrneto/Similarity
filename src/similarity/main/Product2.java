package similarity.main;

import java.util.ArrayList;
import java.util.List;

import similarity.core.Application;
import similarity.core.Criterion;
import similarity.core.Extractor;
import similarity.criterions.ErrorCriterion;
import similarity.elasticSearch.core.ElasticSearchExtractor;
import similarity.perfMiner.core.PerfMinerExtractor;
import similarity.product.errorAlgorithm.ErrorAlgorithm;

public class Product2 {

	public static void main(String[] args) {
		
		List<Extractor> extractors = new ArrayList<Extractor>(); 
		
		extractors.add(new PerfMinerExtractor("PerfMiner-ManualTestsPU"));
		extractors.add(new PerfMinerExtractor("PerfMiner-AutoTestsPU"));
		extractors.add(new ElasticSearchExtractor("arquivo.json"));
		
		List<Criterion> criterions = new ArrayList<Criterion>();
		
		criterions.add(new ErrorCriterion());
		
		Application c = new ErrorAlgorithm(extractors, criterions);
		
		c.execute();
		
	}

}
