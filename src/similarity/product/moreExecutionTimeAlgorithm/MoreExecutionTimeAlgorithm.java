package similarity.product.moreExecutionTimeAlgorithm;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import similarity.core.Application;
import similarity.core.Criterion;
import similarity.core.Extractor;
import similarity.core.entities.Pair;
import similarity.core.entities.Tree;

public class MoreExecutionTimeAlgorithm extends Application{
	
	private List<Pair<Criterion,Integer>> criterions;
	
	private List<Tree> data;
	
	// TODO MAKE THIS METHOD
	public MoreExecutionTimeAlgorithm(List<Extractor> nExtractors, List<Pair<Criterion,Integer>> nCriterions) {
		super(nExtractors);
		
		this.criterions = nCriterions;
	}

	// TODO MAKE THIS METHOD
	@Override
	public void execute() {
		
		//Manual
		this.data = this.getEntities().get(0).getData();
		
		for(Pair<Criterion,Integer> c : this.criterions) {
			
			System.out.println("Applying " + c.getFirst().getName() + " criterion");
			
			this.data = c.getFirst().use(this.data,c.getSecond());
			
			System.out.println("Done");
		}
		
		try {
			File f = this.obtainResults(this.getClass().getSimpleName()+"Results.json");
			System.out.println("Save Results in file : " + f.getAbsolutePath());
		} catch (IOException e) {
			System.err.println("Erro ao salvar as informações no arquivo");
		}
		
		
		int i=1;
		
		for(Tree t : data) {
			System.out.println("Valor : " + i++);
			System.out.println("Nome : " + t.getName());
			System.out.println("Número de execuções : " + t.getNumberExecution());
			System.out.println("Tempo total de execução: " + t.getTotalTimeExecution());
		}
		
	}
	
	// TODO MAKE THIS METHOD
	@Override
	public File obtainResults(String name) throws IOException {
		
		File file = new File(name);
		
		Writer writer = new FileWriter(file);
		
		JsonArray values = new JsonArray(data.size()); 
		
		for(Tree t : this.data) {
			
			JsonObject node = new JsonObject();
			
			node.addProperty("ID", t.getId());
			node.addProperty("NAME", t.getName());
			node.addProperty("NUMBER_EXECUTIONS", t.getNumberExecution());
			node.addProperty("TIME_EXECUTION", t.getTotalTimeExecution());
			
			values.add(node);
		}
		
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String json = gson.toJson(values);
		
		writer.write(json);		
		writer.close();
		
		return file;
		
	}
}
