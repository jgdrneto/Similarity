package similarity.product.similarityAlgorithm;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import similarity.core.Application;
import similarity.core.Extractor;
import similarity.core.entities.Json;
import similarity.core.entities.SimilarNodes;
import similarity.core.entities.SimilarScenarios;
import similarity.core.entities.Tree;
import similarity.core.entities.TreeNode;

public class SimilarityAlgorithm extends Application{

	//Objetos para armazenar cenários em execução
	List<Tree> ManualScenarios = new ArrayList<Tree>();
	List<Tree> AutoScenarios = new ArrayList<Tree>();
	
	// TODO MAKE THIS METHOD
	public SimilarityAlgorithm(List<Extractor> nExtractors) {
		super(nExtractors);
	}
	
	// TODO MAKE THIS METHOD
	@Override
	public void execute() {
		
		//filterScenarios();
		
		/*
		System.out.println("Cen�rios autom�ticos \n");
		for(Tree auto : AutoScenarios) 
		{
			System.out.println("Cenario: " + auto.getName());
			System.out.println("URL: " + auto.getRequestUrl());
			printNodes(auto.getRoot());
			System.out.println("Quantidade de n�s: " + auto.getAmountNodes(auto.getRoot()));
			System.out.println("Altura da �rvore: " + auto.getHeight());
			System.out.println();
		}
		
		System.out.println("Total de registros: " + AutoScenarios.size() + "\n");
		
		
		System.out.println("Cen�rios manuais \n");
		for(Tree manual : ManualScenarios) 
		{
			System.out.println("Cenario: " + manual.getName());
			System.out.println("URL: " + manual.getRequestUrl());
			printNodes(manual.getRoot());
			System.out.println("Quantidade de n�s: " + manual.getAmountNodes(manual.getRoot()));
			System.out.println("Altura da �rvore: " + manual.getHeight());
			System.out.println();
		}
		
		System.out.println("Total de registros: " + ManualScenarios.size());
		*/
		//String urlA, urlB;
		
		ManualScenarios = this.getEntities().get(0).getData();
		AutoScenarios = this.getEntities().get(1).getData();
		
		
		//Ordenando em pr�-ordem para facilitar a busca por similaridade
		for(Tree manual : ManualScenarios) 
		{
			OrdeningNodes(manual.getRoot());
		}
		
		for(Tree auto : AutoScenarios) 
		{
			OrdeningNodes(auto.getRoot());
		}
		
		
		//Funcionalidade para varredura em busca de similaridades
		ArrayList<SimilarScenarios> similarScenarios = new ArrayList<SimilarScenarios>();
		for(Tree manual : ManualScenarios) {
			
			for(Tree auto : AutoScenarios) {
				
				
				//Verifica se existe similaridade entre o nome do cen�rio, URL utilizada e o nome do n� raiz
				if(
						manual.getName().toUpperCase().equals(auto.getName().toUpperCase()) &&
						manual.getRequestUrl() == auto.getRequestUrl() &&
						manual.getRoot().getName().toUpperCase().equals(auto.getRoot().getName().toUpperCase())) 
				{
					List<SimilarNodes> nodes = new ArrayList<SimilarNodes>();
					nodes.addAll(FindSimilarNodes(manual.getRoot(), auto.getRoot()));
					SimilarScenarios scenario = new SimilarScenarios();
					scenario.setIdManual(manual.getId());
					scenario.setIdAuto(auto.getId());
					scenario.setNameManual(manual.getName());
					scenario.setNameAuto(auto.getName());
					scenario.setUrlManual(manual.getRequestUrl());
					scenario.setUrlAuto(auto.getRequestUrl());
					scenario.setAmountNodesManual(manual.getAmountNodes(manual.getRoot()));
					scenario.setAmountNodesAuto(auto.getAmountNodes(auto.getRoot()));
					scenario.setSimilarNodes(nodes);
					scenario.calculateSimilarityScenarios();
				}
				
			}
			
		}
		
		//Imprimir os cen�rios similares
		
		for(SimilarScenarios scenario : similarScenarios) {
			scenario.printSimilarity();
		}
		System.out.println("Total: " + similarScenarios.size() + "\n");
		
		exportResultJson(similarScenarios);
		
		recomenderTests(similarScenarios);
		
		
		
	}
	
	
	//Funcionalidade para ordenar os n�s dos cen�rios em pr�-ordem pelo ID
	private void OrdeningNodes(TreeNode Node) 
	{
		if(Node != null)
	    {
			List<TreeNode> nodes = new ArrayList<TreeNode>();
	        nodes.addAll(Node.getChildrens());
	        Collections.sort(nodes);
	        Node.setChildrens(nodes);
	        for(TreeNode n : Node.getChildrens())
	        {
	          	OrdeningNodes(n);
	        }
	    }
	}
		
		
	//Funcionalidade que faz busca em pr�-ordem para encontrar os n�s similares
	private List<SimilarNodes> FindSimilarNodes(TreeNode NodeManual, TreeNode NodeAuto) 
	{
		//Verificar se os n�s n�o s�o nulos e se os seus nomes s�o iguais
		if(
				NodeManual != null && 
				NodeAuto != null && 
				NodeManual.getName().toUpperCase().equals(NodeAuto.getName().toUpperCase())) 
		{
			/*
			 * Cria uma lista com os n�s similares
			 * -> Armazena o ID do n� contido em cada banco 
			 * -> Armazena o ID do n� pai contido em cada banco
			 * -> Armazena o nome do n�
			 */
			List<SimilarNodes> Nodes = new ArrayList<SimilarNodes>();
			Nodes.add(new SimilarNodes(NodeManual.getId(), NodeAuto.getId(), 
					(NodeManual.getFather() == null ? null : NodeManual.getFather().getId()), 
					(NodeAuto.getFather() == null ? null : NodeAuto.getFather().getId()), NodeManual.getName()));
			
			int x = 0;
			while(x< NodeManual.getChildrens().size() && x< NodeAuto.getChildrens().size()) 
			{
				Nodes.addAll(FindSimilarNodes(NodeManual.getChildrens().get(x), NodeAuto.getChildrens().get(x)));
				x++;	
			}
			
			/*
			for(int x=0; x< NodeManual.getChildrens().size() && x< NodeAuto.getChildrens().size();x++) 
			{
				Nodes.addAll(FindSimilarNodes(NodeManual.getChildrens().get(x), NodeAuto.getChildrens().get(x)));
			}
			*/
			
			return Nodes;
		}else 
		{
			return null;
		}
		
	}
	
	public void recomenderTests(ArrayList<SimilarScenarios> similarScenarios) 
	{
		System.out.println("Testes recomendados para automatiza��o entre 50% e 99%");
		int x=0;
		for(SimilarScenarios scenario : similarScenarios) 
		{
			if(scenario.getPercentualManual() >= 50 && scenario.getPercentualManual() < 100) 
			{
				x++;
				System.out.println("Cen�rio: " + scenario.getNameManual());
				System.out.println("ID manual: " + scenario.getIdManual() + " ID auto: " + scenario.getIdAuto());
				System.out.println("Grau de similaridade: " + scenario.getPercentualManual());
				System.out.println("Url: " + scenario.getUrlManual());
				System.out.println("N�s:\n");
				for(SimilarNodes node : scenario.getSimilarNodes()) 
				{
					System.out.println("Nome: " + node.getNameNode() + " ID: " + node.getIdNodeManual() + " ID parente:" + node.getIdParentManual());
				}
				System.out.println();
			}
		}
		
		System.out.println("Total de cen�rios recomendados para automatiza��o: " + x);
		
	}
	
	
	//Exportar os resultados para o formato Json
	public void exportResultJson(ArrayList<SimilarScenarios> similarScenarios) 
	{
			
		FileWriter arquivo;
		Json conversor = new Json();
		String result;
		try {
			
			arquivo = new FileWriter(new File("D:SimilarScenarios.txt"));
			
			for(SimilarScenarios scenario : similarScenarios) 
			{
				result = conversor.SimilarScenariosToJson(scenario);
				//System.out.println(result);
				arquivo.write(result);
				arquivo.write("\n");
			}
			
			arquivo.close();
		
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
		
		System.out.println();
	}
	
	
	
	//Funcionalidade para imprimir os cen�rios no console
	private void printNodes(TreeNode no) 
	{
		if(no != null)
	    {
	    	System.out.println("Id: " + no.getId() + " N�: " + no.getName() + " Id pai: " + (no.getFather() != null ? no.getFather().getId() : null ));          
	        for(TreeNode n : no.getChildrens())
	        {
	        	printNodes(n);
	        }
	    }
	}
	
	
	//Funcionalidade para filtrar os cen�rios manuais e autom�ticos
	/*	
	private void filterScenarios() 
		{
			boolean contains;
			String tupla1, tupla2;
			ArrayList<String> tuplas = new ArrayList<String>();
			
			
			//Filtragem dos cen�rios manuais	
			for(Tree manual : this.getEntities().get(0).getData()) {
				contains = false;
				if(ManualScenarios.size()==0) {
					ManualScenarios.add(manual);
				}else {	
					for (Tree t : ManualScenarios){
						
						//Verifica se o nome do cen�rio, nome da URL e o nome do n� s�o iguais. Caso seja verdadeiro o cen�rio duplicado � descartado da lista
						tuplas.add(t.getName().substring(t.getName().lastIndexOf(" "), t.getName().length()));
						tuplas.add(manual.getName().substring(manual.getName().lastIndexOf(" "), manual.getName().length()));
						tuplas.add(t.getRequestUrl() == null ? "nulo" : t.getRequestUrl());
						tuplas.add(manual.getRequestUrl() == null ? "nulo" : manual.getRequestUrl());
						tuplas.add(t.getRoot().getName());
						tuplas.add(manual.getRoot().getName());
						if(
								tuplas.get(0).equals(tuplas.get(1)) &&
								tuplas.get(2).equals(tuplas.get(3)) &&
								tuplas.get(4).equals(tuplas.get(5))) {
							contains = true;
							tuplas.clear();
							break;
						}
						tuplas.clear();	
							
					}
					
					if(!contains) {
						ManualScenarios.add(manual);
					}
				}	
			}
			
			
			//Ordena as �rvores dos cen�rios manuais em pr�-ordem
			for(Tree manual : ManualScenarios) 
			{
				OrdeningNodes(manual.getRoot());
			}
			
			
			//Filtragem dos cen�rios autom�ticos
			for(Tree auto : this.getEntities().get(1).getData()) {
				contains = false;
				if(AutoScenarios.size()==0) {
					AutoScenarios.add(auto);
				}else {	
					for (Tree t : AutoScenarios){
						
						//Verifica se o nome do cen�rio, nome da URL e o nome do n� s�o iguais. Caso seja verdadeiro o cen�rio duplicado � descartado da lista
						tuplas.add(t.getName().substring(t.getName().lastIndexOf(" "), t.getName().length()));
						tuplas.add(auto.getName().substring(auto.getName().lastIndexOf(" "), auto.getName().length()));
						tuplas.add(t.getRequestUrl() == null ? "nulo" : t.getRequestUrl());
						tuplas.add(auto.getRequestUrl() == null ? "nulo" : auto.getRequestUrl());
						tuplas.add(t.getRoot().getName());
						tuplas.add(auto.getRoot().getName());
						if(
								tuplas.get(0).equals(tuplas.get(1)) &&
								tuplas.get(2).equals(tuplas.get(3)) &&
								tuplas.get(4).equals(tuplas.get(5))) {
							contains = true;
							tuplas.clear();
							break;
						}
						tuplas.clear();
					}
		
					if(!contains) {
						AutoScenarios.add(auto);
					}
				}	
			}
			
			//Ordenando as �rvores dos cen�rios autom�ticos em pr�-ordem
			for(Tree auto : AutoScenarios) 
			{
				OrdeningNodes(auto.getRoot());
			}
				
					
		}
	*/
	
	// TODO MAKE THIS METHOD
	@Override
	public File obtainResults(String name) throws IOException{
		return null;
	}
}

