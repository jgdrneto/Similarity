package similarity.core;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import similarity.core.entities.Entity;
import similarity.core.entities.Json;

public abstract class Application {
	
	public static int QUANT_REGS_FROM_DATABASE = 512; 
	
	private List<Entity> entities;
	
	public Application(List<Extractor> nExtractors){
		entities = new ArrayList<Entity>();
		
		//Condi��o para o produto 1
		if(nExtractors.size()==2) 
		{
			entities.add(nExtractors.get(0).extractRegister(5000,0));
			entities.add(nExtractors.get(1).extractRegister(512,0));
			
		}else 
		{
			for(Extractor e : nExtractors) {
				entities.add(e.extractRegister(QUANT_REGS_FROM_DATABASE,0));
			}
		}
		
		
		
		
	}
	
	public List<Entity> getEntities() {
		return entities;
	}

	public void setEntities(List<Entity> entities) {
		this.entities = entities;
	}
	
	abstract public void execute();
	
	abstract public File obtainResults(String name)throws IOException;
	
	//TODO Make this method
	public Json obtainData() {
		return new Json();
	}
}
