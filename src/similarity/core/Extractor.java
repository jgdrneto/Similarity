package similarity.core;

import similarity.core.entities.Entity;

public abstract class Extractor {
	
	private String name;
	
	public Extractor(String nName){
		this.name = nName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
		
	abstract public Entity extractRegister();
	
	abstract public Entity extractRegister(int quant, int initRegister);

}
