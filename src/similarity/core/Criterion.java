package similarity.core;

import java.util.List;

import similarity.core.entities.Tree;

public interface Criterion {
	
	public abstract List<Tree> use(List<Tree> data, int metric);
	
	public abstract String getName(); 
}
