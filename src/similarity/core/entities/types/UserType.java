package similarity.core.entities.types;

import java.util.ArrayList;
import java.util.List;

public class UserType{
	
	private String name;
	private List<PrimitiveType> primitives;
	private List<UserType> objects;
	
	public UserType() {
		name = "";
		primitives= new ArrayList<PrimitiveType>();
		objects= new ArrayList<UserType>();
	}
	
	public UserType(String nName, List<PrimitiveType> nPrimitives, List<UserType> nObjects) {
		super();
		this.name = nName;
		this.primitives = nPrimitives;
		this.objects = nObjects;
	}

	//TODO MAKE THIS METHOD
	List<PrimitiveType> getAllDescendantsPrimitiveTypes(){
		return new ArrayList<PrimitiveType>();
	}
	
	//TODO MAKE THIS METHOD
	List<UserType> getAllDescendantsUserTypes(){
		return new ArrayList<UserType>();
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof UserType){
			UserType obj2 = (UserType)obj;
			
			return this.name.equals(obj2.name) &&
				   this.primitives.equals(obj2.primitives) &&
				   this.objects.equals(obj2.objects);
		}
		
		return false;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public List<PrimitiveType> getPrimitives() {
		return primitives;
	}
	
	public void setPrimitives(List<PrimitiveType> primitives) {
		this.primitives = primitives;
	}
	
	public List<UserType> getObjects() {
		return objects;
	}
	
	public void setObjects(List<UserType> objects) {
		this.objects = objects;
	}
	
}
