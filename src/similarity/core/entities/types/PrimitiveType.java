package similarity.core.entities.types;

public class PrimitiveType{

	private String value;
	private String name;
	private String type;

	public PrimitiveType(String nValue, String nName, String nType) {
		this.value = nValue;
		this.name = nName;
		this.type = nType;
	}
	
	@Override
	public boolean equals(Object obj) {
		
		if(obj instanceof PrimitiveType){
			PrimitiveType obj2 = (PrimitiveType)obj;
			
			return this.name.equals(obj2.name) &&
				   this.type.equals(obj2.type) &&
				   this.value.equals(obj2.value);
		}
		
		return false;

	}

	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
}
