package similarity.core.entities;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class MethodExceptionsConverter implements AttributeConverter<MethodExceptions, String>{

	@Override
	public String convertToDatabaseColumn(MethodExceptions arg0) {
		
		return arg0.toTypeString();

	}

	@Override
	public MethodExceptions convertToEntityAttribute(String data) {
		MethodExceptions m = new MethodExceptions();
		
		if(data != null){
			String[] str = data.split(" ");
			//Recupera todas as exceções e armazena em uma pilha na ordem da última até a primeira exceção
			for(int x=0; x<str.length; x++)
		    {
				if(str[x].toUpperCase().contains("EXCEPTION") && str[x].length() > 9)
		        {
					m.getType().add(str[x].replace(":", "").replace("(", "").replace(")", ""));
		        }    
		    }
		}
		
		return m;
	}

}
