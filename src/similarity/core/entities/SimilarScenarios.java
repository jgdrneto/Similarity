package similarity.core.entities;

import java.util.List;

public class SimilarScenarios {

	private String nameManual, nameAuto, urlManual, urlAuto;
	private long idManual, idAuto;
	private int amountNodesManual, amountNodesAuto;
	private List<SimilarNodes> similarNodes;
	private double percentualManual, percentualAuto;
	
	public void setSimilarNode(SimilarNodes n) 
	{
		getSimilarNodes().add(n);
	}
	
	//Calcular a similaridade em rela��o ao cen�rio Manual e em rela��o ao cen�rio Autom�tico
	public void calculateSimilarityScenarios() 
	{
		
		setPercentualManual((getSimilarNodes().size()*100)/amountNodesManual);
		setPercentualAuto((getSimilarNodes().size()*100)/amountNodesAuto);
	}
	
	
	//Impress�o do cen�rio
		public void printSimilarity() 
		{
			System.out.println("Nome do cen�rio: " + this.nameManual);
			System.out.println("URL: " + this.urlManual);
			System.out.println("Quantidade de n�s similares: " + this.getSimilarNodes().size());
			for(SimilarNodes n : this.getSimilarNodes()) 
			{
				System.out.println("Nome: " + n.getNameNode() + " \nID manual: " + n.getIdNodeManual() + " ID auto: " + n.getIdNodeAuto()
				+ " ID pai manual: " + n.getIdParentManual() + " ID pai auto: " + n.getIdParentAuto());
			}
			System.out.println("Quantidade de n�s na base manual: " + this.amountNodesManual);
			System.out.println("Quantidade de n�s na base autom�tico: " + this.amountNodesAuto);
			System.out.println("Similaridade do autom�tico para o manual: " + this.percentualManual + "%");
			System.out.println("Similaridade do manual para o autom�tico: " + this.percentualAuto + "%");
			System.out.println();
		}

	@Override
	public boolean equals(Object obj) 
	{
		if(obj instanceof SimilarScenarios) {
			
			SimilarScenarios obj2 = (SimilarScenarios)obj;
			
			return this.nameManual.equals(obj2.nameManual)					&&
				   this.urlManual == obj2.urlManual					&&
				   this.similarNodes.containsAll(obj2.similarNodes);
		}
		
		return false;
	}
		
	public long getIdManual() {
		return idManual;
	}

	public void setIdManual(long idManual) {
		this.idManual = idManual;
	}

	public long getIdAuto() {
		return idAuto;
	}

	public void setIdAuto(long idAuto) {
		this.idAuto = idAuto;
	}
	
	public String getNameManual() {
		return nameManual;
	}

	public void setNameManual(String nameManual) {
		this.nameManual = nameManual;
	}

	public String getNameAuto() {
		return nameAuto;
	}

	public void setNameAuto(String nameAuto) {
		this.nameAuto = nameAuto;
	}

	public String getUrlManual() {
		return urlManual;
	}

	public void setUrlManual(String urlManual) {
		this.urlManual = urlManual;
	}

	public String getUrlAuto() {
		return urlAuto;
	}

	public void setUrlAuto(String urlAuto) {
		this.urlAuto = urlAuto;
	}

	public int getAmountNodesManual() {
		return amountNodesManual;
	}

	public void setAmountNodesManual(int amountNodesManual) {
		this.amountNodesManual = amountNodesManual;
	}

	public int getAmountNodesAuto() {
		return amountNodesAuto;
	}

	public void setAmountNodesAuto(int amountNodesAuto) {
		this.amountNodesAuto = amountNodesAuto;
	}

	public List<SimilarNodes> getSimilarNodes() {
		return similarNodes;
	}

	public void setSimilarNodes(List<SimilarNodes> similarNodes) {
		this.similarNodes = similarNodes;
	}

	public double getPercentualManual() {
		return percentualManual;
	}

	public void setPercentualManual(double percentualManual) {
		this.percentualManual = percentualManual;
	}

	public double getPercentualAuto() {
		return percentualAuto;
	}

	public void setPercentualAuto(double percentualAuto) {
		this.percentualAuto = percentualAuto;
	}
	
	
}
