package similarity.core.entities;
import java.io.BufferedReader;

import com.google.gson.Gson;

public class Json {

	private Gson gson = new Gson();
	
	public ElasticSearchLog JsonToLog(BufferedReader br)
	{	
        //Converter um Buffer de arquivo JSON para objeto Java ElasticSearchLog
		ElasticSearchLog obj = gson.fromJson(br, ElasticSearchLog.class);
		return obj;
    }
	
	public String LogToJson(ElasticSearchLog esl) 
	{	 
		//Converte objetos Java, em formato JSON, para String
		return gson.toJson(esl);
	} 
	
	public String SimilarScenariosToJson(SimilarScenarios ss) 
	{
		//Converte objetos Java, em formato JSON, para String
		return gson.toJson(ss, SimilarScenarios.class);
	}
	
}
