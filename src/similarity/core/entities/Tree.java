/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package similarity.core.entities;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.persistence.annotations.InstantiationCopyPolicy;

@Entity
@InstantiationCopyPolicy
@Table(name = "scenario")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tree.findAll", query = "SELECT a FROM Tree a")
    , @NamedQuery(name = "Tree.findById", query = "SELECT a FROM Tree a WHERE a.id = :id")
    , @NamedQuery(name = "Tree.findByDate", query = "SELECT a FROM Tree a WHERE a.date = :date")
    , @NamedQuery(name = "Tree.findByName", query = "SELECT a FROM Tree a WHERE a.name = :name")
    , @NamedQuery(name = "Tree.findByRequestParameters", query = "SELECT a FROM Tree a WHERE a.requestParameters = :requestParameters")
    , @NamedQuery(name = "Tree.findByRequestUrl", query = "SELECT a FROM Tree a WHERE a.requestUrl = :requestUrl")})
public class Tree implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    
    @Column(name = "date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;
    
    @Column(name = "name")
    private String name;
    
    @Column(name = "request_parameters")
    private String requestParameters;
    
    @Column(name = "request_url")
    private String requestUrl;
    
    @JoinColumn(name = "execution_id", referencedColumnName = "id")
    @ManyToOne
    private Execution execution;
    
    @JoinColumn(name = "root_id", referencedColumnName = "id")
    @ManyToOne
    private TreeNode root;
    
    @Transient
	private int numberExecution;
    
    public Tree() {
		this.root = null;
		this.name = "";
		this.requestUrl = "null";
		this.date = new Date(0);
		this.execution = new Execution();
		this.requestParameters = "";
		this.numberExecution=1;
	}
	
	public Tree(TreeNode nRoot, String nName) {
		this.root = nRoot;
		this.name = nName;
		this.requestUrl = "null";
		this.date = new Date(0);
		this.execution = new Execution();
		this.requestParameters = "";
		this.numberExecution=1;
	}
	
	public Tree(long nId,TreeNode nRoot, String nName, String nRequestUrl, Date nDateExecution, Execution nExecution, String nRequestParameters) {
		this.id = nId;
		this.root = nRoot;
		this.name = nName;
		this.requestUrl = nRequestUrl == null ? "null" : nRequestUrl;
		this.date = nDateExecution;
		this.execution = nExecution;
		this.requestParameters =  nRequestParameters;
		this.numberExecution=1;
	}
	
	public Tree(long nId,TreeNode nRoot, String nName, String nUrlRequest, Date nDateExecution, Execution nExecution, String nRequestParameters, int nNumberExecution) {
		this.id = nId;
		this.root = nRoot;
		this.name = nName;
		this.requestUrl = nUrlRequest == null ? "null" : nUrlRequest;
		this.date = nDateExecution;
		this.execution = nExecution;
		this.requestParameters =  nRequestParameters;
		this.numberExecution=nNumberExecution;
	}
	
	public int getHeight() {	
		return auxGetHeight(this.root);
	}
		
	public int auxGetHeight(TreeNode node) {
		if (node == null) {
	        return 0;
		}else {
			List<Integer> hs = new ArrayList<Integer>();
			hs.add(0);
			for(TreeNode tn : node.getChildrens()) {
				hs.add(auxGetHeight(tn));
			}
			
	        return Collections.max(hs) + 1;
		}
	}
	
	public BigInteger getTotalTimeExecution() {	
		if(this.root==null) {
			return BigInteger.ZERO;
		}else {
			return root.getTime();
		}
	}
	
	
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRequestParameters() {
        return requestParameters;
    }

    public void setRequestParameters(String requestParameters) {
        this.requestParameters = requestParameters;
    }

    public String getRequestUrl() {
        return requestUrl;
    }

    public void setRequestUrl(String requestUrl) {
        this.requestUrl = requestUrl;
    }

    public Execution getExecution() {
        return execution;
    }

    public void setExecution(Execution nExecution) {
        this.execution = nExecution;
    }

    public TreeNode getRoot() {
        return this.root;
    }

    public void setRoot(TreeNode nRoot) {
        this.root = nRoot;
    }

    public int getNumberExecution() {
		return numberExecution;
	}

	public void setNumberExecution(int numberExecution) {
		this.numberExecution = numberExecution;
	}

	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Tree) {
			
			Tree obj2 = (Tree)obj;
			
			return this.name.equals(obj2.name)								&&
				   this.requestUrl.equals(obj2.requestUrl)					&&
				   this.requestParameters.equals(obj2.requestParameters)	&&
				   this.root.equals(obj2.root);
		}
		
		return false;
	}
	
	@Override
	public String toString() {
		return "Tree [" + "\n"
				+ "id=" + id + "\n" 
				+ "root=" + root + "\n"
				+ "name=" + name + "\n" 
				+ "urlRequest=" + requestUrl + "\n"
				+ "dateExecution=" + date + "\n"
				+ "execution=" + execution + "\n"
				+ "parameters=" + requestParameters + "\n"
				+ "]";
	}
    
	public int getAmountNodes(TreeNode n) 
	{
		/*
		if(n != null) 
		{
			this.setNumberNodes(this.getNumberNodes()+1);
			for(TreeNode childrens : n.getChildrens())
            {
                calculateNodes(childrens);
            }
		}
		*/
		if (n == null) {
	        return 0;
		}else {
			int count = 1;
			for(TreeNode tn : n.getChildrens()) {
				count += getAmountNodes(tn);
			}
			return count;
		}
	        
	}
   
    
}
