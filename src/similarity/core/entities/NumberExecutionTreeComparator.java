package similarity.core.entities;

import java.util.Comparator;

public class NumberExecutionTreeComparator implements Comparator<Tree>{

	@Override
	public int compare(Tree t1, Tree t2) {
		
		if(t1.getNumberExecution()==t2.getNumberExecution()) {
			return 0;
		}else {
			if(t1.getNumberExecution()>t2.getNumberExecution()) {
				return -1;
			}else {
				return 1;
			}
		}
	}

}
