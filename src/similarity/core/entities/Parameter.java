package similarity.core.entities;

import java.util.ArrayList;
import java.util.List;

import similarity.core.entities.types.PrimitiveType;
import similarity.core.entities.types.UserType;

public class Parameter {
	
	private List<UserType> objects;
	private List<PrimitiveType> primitives;
	
	public Parameter() {
		this.objects = new ArrayList<UserType>();
		this.primitives = new ArrayList<PrimitiveType>();
	}
	
	public Parameter(String data) {
		this.objects = new ArrayList<UserType>();
		this.primitives = new ArrayList<PrimitiveType>();
		
		if(data==null) {
			data="";
		}
		
		this.obtainValues(data);
	}
	
	@Override
	public boolean equals(Object obj) {
		
		if(obj instanceof Parameter){
			
			Parameter obj2 = (Parameter)obj;
			
			return this.objects.equals(obj2.objects) &&
				   this.primitives.equals(obj2.primitives);
		}
		
		return false;
	}
	
	// TODO MAKE THIS METHOD
	private void obtainValues(String data) {
		
	}

}
