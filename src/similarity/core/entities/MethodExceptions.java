package similarity.core.entities;

import java.util.ArrayList;

public class MethodExceptions {
	
	private ArrayList<String> type;
	private String menssage;
	
	public MethodExceptions() {
		this.type = new ArrayList<String>();
		this.menssage="";
	}
	
	public MethodExceptions(String data) {
		this.type = new ArrayList<String>();
		this.menssage="";
		
		this.obtainValues(data);
	}
	
	private void obtainValues(String data) {

		/*
		 * OBS: Tanto o banco de dados de registros de testes manuais quanto o banco de dados de registros de testes automáticos 
		 * possuem poucos registros de testes com erros. Os registros de exceções são ligados diretamente aos registros de nós: 
		 * 
		 * Banco Automático: 3 registros
		 * Banco Manual: 5696
		 * 
		 * Se nós cuja coluna "exception" seja nula provalmente indica que aquele nó não causou exceção em sua execução. Ou seja, não disparou erro.
		 * Então, em termos de cenarios:
		 * 
		 * Banco Automático: 1 cenário
		 * Banco Manual: 149 cenários
		 * 
		 */
		if(data != null){
			String[] str = data.split(" ");
			//Recupera todas as exceções e armazena em uma pilha na ordem da última até a primeira exceção
			for(int x=0; x<str.length; x++)
		    {
				if(str[x].toUpperCase().contains("EXCEPTION") && str[x].length() > 9)
		        {
					type.add(str[x].replace(":", "").replace("(", "").replace(")", ""));
		        }    
		    }
		}
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof MethodExceptions){
			
			MethodExceptions obj2 = (MethodExceptions)obj;
			
			return this.type.equals(obj2.type) &&
				   this.menssage.equals(obj2.menssage);
		}
		
		return false;
	}

	@Override
	public String toString() {
		return "MethodExceptions [type=" + this.type + ", menssage=" + this.menssage + "]";
	}

	public String toTypeString() {
		String s="";
		
		if(this.type.size()==1) {
			s = this.type.get(0);
		}else {
			for(int i=0;i<this.type.size();i++) {
				if(i+1!=this.type.size()) {
					s+= (this.type.get(i) + " | ");
				}else {
					s+=this.type.get(i);
				}
			}
		}	
		return s;
	}
	
	public ArrayList<String> getType() {
		return type;
	}

	public void addType(String type) {
		this.type.add(type);
	}
	
	public void setType(ArrayList<String> type) {
		this.type = type;
	}

	public String getMenssage() {
		return menssage;
	}

	public void setMenssage(String menssage) {
		this.menssage = menssage;
	}
}
