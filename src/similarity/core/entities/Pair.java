package similarity.core.entities;

public class Pair<F,S> {
	
	private F first;
	private S second;
	
	public Pair(F nFirst, S nSecond) {
		this.first = nFirst;
		this.second = nSecond;
	}

	public F getFirst() {
		return first;
	}

	public void setFirst(F nFirst) {
		this.first = nFirst;
	}

	public S getSecond() {
		return second;
	}

	public void setSecond(S nSecond) {
		this.second = nSecond;
	}
	
}
