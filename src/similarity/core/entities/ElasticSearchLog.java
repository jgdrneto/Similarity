package similarity.core.entities;

public class ElasticSearchLog {

	private String _index;
    private String _type;
    private String _id;
    private String _score;
    private Source _source;

    public String getIndex() {
        return _index;
    }

    public void setIndex(String _index) {
        this._index = _index;
    }

    public String getType() {
        return _type;
    }

    public void setType(String _type) {
        this._type = _type;
    }

    public String getId() {
        return _id;
    }

    public void setId(String _id) {
        this._id = _id;
    }

    public String getScore() {
        return _score;
    }

    public void setScore(String _score) {
        this._score = _score;
    }

    public Source getSource() {
        return _source;
    }

    public void setSource(Source _source) {
        this._source = _source;
    }
	
	
}
