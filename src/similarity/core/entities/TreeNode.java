/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package similarity.core.entities;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "node")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TreeNode.findAll", query = "SELECT a FROM TreeNode a")
    , @NamedQuery(name = "TreeNode.findById", query = "SELECT a FROM TreeNode a WHERE a.id = :id")
    , @NamedQuery(name = "TreeNode.findByTime", query = "SELECT a FROM TreeNode a WHERE a.time = :time")
    , @NamedQuery(name = "TreeNode.findByRealTime", query = "SELECT a FROM TreeNode a WHERE a.realTime = :realTime")})
public class TreeNode implements Serializable,Comparable<TreeNode>  {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    
    @Column(name = "exception")
    @Convert(converter = MethodExceptionsConverter.class)
    private MethodExceptions exceptions;
    
    @Column(name = "time")
    private BigInteger time;
    
    @Column(name = "member")
    private String name;
    
    @Column(name = "real_time")
    private BigInteger realTime;
    
    @OneToMany(mappedBy = "father")
    private List<TreeNode> childrens;
    
    @JoinColumn(name = "parent_id", referencedColumnName = "id")
    @ManyToOne
    private TreeNode father;
    
    public TreeNode() {
		this.name = "";
		this.id = new Long(0);
		this.father = null;
		this.time = BigInteger.ZERO;
		this.realTime = BigInteger.ZERO;
		this.exceptions = new MethodExceptions();
		this.childrens = new ArrayList<TreeNode>();
	}
	
	public TreeNode(String nName) {
		this.name = nName;
		this.id = new Long(0);
		this.father = null;
		this.time = BigInteger.ZERO;
		this.realTime = BigInteger.ZERO;
		this.exceptions = new MethodExceptions();
		this.childrens = new ArrayList<TreeNode>();
	}
	
	public TreeNode(String nName, long nId) {
		this.name = nName;
		this.id = nId;
		this.father = null;
		this.time = BigInteger.ZERO;
		this.realTime = BigInteger.ZERO;
		this.exceptions = new MethodExceptions();
		this.childrens = new ArrayList<TreeNode>();
	}
	
	public TreeNode(String nName, long nId, TreeNode nFather, BigInteger nTime, BigInteger nRealTime, MethodExceptions nExceptions,
			Parameter nParameter, List<TreeNode> nChildrens) {
		this.name = nName;
		this.id = nId;
		this.father = nFather;
		this.time = nTime;
		this.realTime = nRealTime;
		this.exceptions = nExceptions;
		this.childrens = nChildrens;
	}

    
    public void addChildren(TreeNode n) {
		this.childrens.add(n);
		n.father=this;
	}
    
    @Override
    public int compareTo(TreeNode t) {
        if (this.id > t.getId()) {
            return 1;
        }
        else if (this.id < t.getId()) {
            return -1;
        }
        return 0;
    }

    @Override
	public boolean equals(Object obj) {
		
		if(obj instanceof TreeNode) {
			
			TreeNode obj2 = (TreeNode)obj;
			
			return this.name.equals(obj2.name)				&& 
				   this.exceptions.equals(obj2.exceptions)	&&
				   this.childrens.equals(obj2.childrens);	
		}
		
		return false;
	}
    
    
    //Get's and Set's
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public MethodExceptions getExceptions() {
        return this.exceptions;
    }

    public void setExceptions(MethodExceptions nException) {
        this.exceptions = nException;
    }

    public BigInteger getTime() {
        return time;
    }

    public void setTime(BigInteger time) {
        this.time = time;
    }

    public String getName() {
        return name;
    }

    public void setName(String nName) {
        this.name = nName;
    }

    public BigInteger getRealTime() {
        return realTime;
    }

    public void setRealTime(BigInteger realTime) {
        this.realTime = realTime;
    }

    @XmlTransient
    public List<TreeNode> getChildrens() {
        return this.childrens;
    }

    public void setChildrens(List<TreeNode> nChildrens) {
        
    	for(TreeNode e : nChildrens) {
    		e.setFather(this);
    	}
    	
    	this.childrens = nChildrens;
    }

    public TreeNode getFather() {
        return father;
    }

    public void setFather(TreeNode nFather) {
        this.father = nFather;
    }    
    
    
}
