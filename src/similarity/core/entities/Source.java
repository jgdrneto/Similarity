package similarity.core.entities;

public class Source {

    private String message;
    private String data_hora_operacao;
	private String url;
    private String tempo;
    private int id_sistema;
    private String id_registro_entrada;
    private String erro;
    private String parametros;

	
    String getMessage() {
		return message;
	}
	void setMessage(String message) {
		this.message = message;
	}
    
	public String getData_hora_operacao() {
		return data_hora_operacao;
	}
	public void setData_hora_operacao(String data_hora_operacao) {
		this.data_hora_operacao = data_hora_operacao;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getTempo() {
		return tempo;
	}
	public void setTempo(String tempo) {
		this.tempo = tempo;
	}
	public int getId_sistema() {
		return id_sistema;
	}
	public void setId_sistema(int id_sistema) {
		this.id_sistema = id_sistema;
	}
	public String getId_registro_entrada() {
		return id_registro_entrada;
	}
	public void setId_registro_entrada(String id_registro_entrada) {
		this.id_registro_entrada = id_registro_entrada;
	}
	public String getErro() {
		return erro;
	}
	public void setErro(String erro) {
		this.erro = erro;
	}
	public String getParametros() {
		return parametros;
	}
	public void setParametros(String parametros) {
		this.parametros = parametros;
	}
	
}