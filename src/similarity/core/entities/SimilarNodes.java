package similarity.core.entities;

public class SimilarNodes {

	private Long idNodeManual, idNodeAuto, idParentManual, idParentAuto;
	private String nameNode;
	
	public SimilarNodes(Long idNodeManual, Long idNodeAuto, Long idParentManual, Long idParentAuto, String nameNode)
	{
		this.idNodeManual = idNodeManual;
		this.idNodeAuto = idNodeAuto;
		this.idParentManual = idParentManual;
		this.idParentAuto = idParentAuto;
		this.nameNode = nameNode;
	}
	
	
	public Long getIdNodeManual() {
		return idNodeManual;
	}
	public void setIdNodeManual(Long idNodeManual) {
		this.idNodeManual = idNodeManual;
	}
	public Long getIdNodeAuto() {
		return idNodeAuto;
	}
	public void setIdNodeAuto(Long idNodeAuto) {
		this.idNodeAuto = idNodeAuto;
	}
	public Long getIdParentManual() {
		return idParentManual;
	}
	public void setIdParentManual(Long idParentManual) {
		this.idParentManual = idParentManual;
	}
	public Long getIdParentAuto() {
		return idParentAuto;
	}
	public void setIdParentAuto(Long idParentAuto) {
		this.idParentAuto = idParentAuto;
	}
	public String getNameNode() {
		return nameNode;
	}
	public void setNameNode(String nameNode) {
		this.nameNode = nameNode;
	}
	
	
	
}
