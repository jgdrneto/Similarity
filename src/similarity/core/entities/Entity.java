package similarity.core.entities;

import java.util.List;

public class Entity {
	
	String name;
	List<Tree> data;
	
	public Entity(String nName, List<Tree> nData) {
		this.name = nName;
		this.data = nData;
	}
	
	//GET'S AND SET'S
	
	@Override
	public String toString() {
		return "Entity [name=" + name + ", data=" + data + "]";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Tree> getData() {
		return data;
	}

	public void setData(List<Tree> data) {
		this.data = data;
	}

}
