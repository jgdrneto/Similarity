package similarity.perfMiner.core;

import java.util.List;

import similarity.core.Extractor;
import similarity.core.entities.Entity;
import similarity.core.entities.Tree;
import similarity.perfMiner.extractor.controllers.ScenarioJPAController;

public class PerfMinerExtractor extends Extractor{
	
	private ScenarioJPAController scenario;
	
	public PerfMinerExtractor(String nName) {
		super(nName);
		
		this.scenario = new ScenarioJPAController(nName);
	}

	@Override
	public Entity extractRegister() {

        List<Tree> trees = this.scenario.findEntities();
        
        for(Tree t : trees) {
        	if(t.getRequestUrl()==null) {
        		t.setRequestUrl("null");
        	}
        	if(t.getRequestParameters()==null) {
        		t.setRequestParameters("null");
        	}
        }
        
		return new Entity(this.getName(),trees);
	}
	
	@Override
	public Entity extractRegister(int quant, int initRegister) {
        
		List<Tree> trees = this.scenario.findEntities(quant,initRegister);
        
		for(Tree t : trees) {
        	if(t.getRequestUrl()==null) {
        		t.setRequestUrl("null");
        	}
        	if(t.getRequestParameters()==null) {
        		t.setRequestParameters("null");
        	}
        }
		
		return new Entity(this.getName(),trees);
		
	}
}
