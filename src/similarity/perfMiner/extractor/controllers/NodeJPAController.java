/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package similarity.perfMiner.extractor.controllers;

import similarity.core.entities.TreeNode;

import java.io.Serializable;

/**
 *
 * @author Jullian
 */
public class NodeJPAController extends JPAController<TreeNode> implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NodeJPAController(String nName) {
        super(nName,TreeNode.class);
    }

}
