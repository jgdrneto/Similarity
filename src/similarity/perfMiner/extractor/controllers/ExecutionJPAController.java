/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package similarity.perfMiner.extractor.controllers;

import similarity.core.entities.Execution;
import similarity.perfMiner.entity.Scenario;
import similarity.perfMiner.extractor.controllers.exceptions.NonexistentEntityException;

import java.io.Serializable;
import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;

/**
 *
 * @author Jullian
 */
public class ExecutionJPAController extends JPAController<Execution> implements Serializable {

	private static final long serialVersionUID = 1L;

	public ExecutionJPAController(String nName) {
        super(nName,Execution.class);
    }
   
}
