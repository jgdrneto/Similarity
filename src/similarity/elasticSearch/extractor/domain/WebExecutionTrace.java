package similarity.elasticSearch.extractor.domain;

import java.util.List;

public interface WebExecutionTrace extends Log {
	public List<TraceElement> getTraceElements();
	public boolean contains(TraceElement traceElement);
	public boolean contains(List<TraceElement> traceElements);
	public boolean equivalent(WebExecutionTrace other);
	public String getExceptionMessage();
	public String getTraceElementsAsString();
	public Integer getHeight();
	public Integer getQtdTraceElements();
}
