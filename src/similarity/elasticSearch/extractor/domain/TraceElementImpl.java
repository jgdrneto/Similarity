package similarity.elasticSearch.extractor.domain;

import java.util.ArrayList;
import java.util.List;


public class TraceElementImpl extends AbstractTraceElement implements TraceElement {
	private String signature;
	private TraceElementImpl parent;
	private List<TraceElementImpl> children = new ArrayList<>();;
	
	public TraceElementImpl() {
	}
	
	public TraceElementImpl(String signature) {
		this.signature = signature;
	}
	
	public void addChild(TraceElementImpl traceElement) {
		traceElement.setParent(this);
		children.add(traceElement);
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public TraceElementImpl getParent() {
		return parent;
	}

	public void setParent(TraceElementImpl parent) {
		this.parent = parent;
	}

	public List<TraceElementImpl> getChildren() {
		return children;
	}

	public void setChildren(List<TraceElementImpl> children) {
		this.children = children;
	}
}
