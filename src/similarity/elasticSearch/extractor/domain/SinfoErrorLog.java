package similarity.elasticSearch.extractor.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;



public class SinfoErrorLog extends AbstractWebExcecutionTrace implements WebExecutionTrace {
	private SinfoLog basicLog;
	private String idSubsistema;
	private String exceptionMessage;
	private String classeExcecao;
	private Long codigoProcessador;
	private String loginUsuario;
	private String rootCause;
	
	private List<StackTraceElement> stackTraceElements;
	private List<TraceElement> traceElements;
	
	public SinfoErrorLog(SinfoLog basicLog, String idSubsistema,
			String exceptionMessage, String classeExcecao, Long codigoProcessador, String loginUsuario,
			String rootCause, List<StackTraceElement> stackTraceElements) {
		this.basicLog = basicLog;
		this.idSubsistema = idSubsistema;
		this.exceptionMessage = exceptionMessage;
		this.classeExcecao = classeExcecao;
		this.codigoProcessador = codigoProcessador;
		this.loginUsuario = loginUsuario;
		this.rootCause = rootCause;
		this.stackTraceElements = stackTraceElements;
	}

	@Override
	public String getRequestURL() {
		return basicLog.getRequestURL();
	}
	
	@Override
	public String getIdentifier() {
		return basicLog.getIdentifier();
	}
	
	@Override
	public Date getDate() {
		return basicLog.getDate();
	}

	@Override
	public LogType getLogType() {
		return LogType.ERROR;
	}
	
	@Override
	public Map<String, String> getMoreInformation() {
		moreInformation = basicLog.getMoreInformation();
		if(idSubsistema != null) {
			moreInformation.put("id_subsistema", idSubsistema);
		}
		if(loginUsuario != null) {
			moreInformation.put("login_usuario", loginUsuario);
		}
		if(classeExcecao != null) {
			moreInformation.put("classe_excecao", classeExcecao);
		}
		if(codigoProcessador != null) {
			moreInformation.put("codigo_processador", codigoProcessador.toString());
		}
		if(rootCause != null) {
			moreInformation.put("root_cause", rootCause);
		}
		
		return moreInformation;
	}

	@Override
	public List<TraceElement> getTraceElements() {
		if(traceElements == null) {
			traceElements = new ArrayList<>();
			if(stackTraceElements != null && !stackTraceElements.isEmpty()) {
				TraceElementImpl parent = toTraceElement(stackTraceElements.get(stackTraceElements.size()-1));
				traceElements.add(parent);
				for(int i = stackTraceElements.size()-2; i >= 0; i--) {
					TraceElementImpl child = toTraceElement(stackTraceElements.get(i));
					parent.addChild(child);
					parent = child;
				}
			}
		}
		return traceElements;
	}
	
	private TraceElementImpl toTraceElement(StackTraceElement ste) {
		TraceElementImpl te= new TraceElementImpl();
		String signature = ste.getClassName();
		//Verifica se o método é um construtor e ignora, pois o PerfMiner não guarda essa informação no banco
		if(!ste.getMethodName().equals("<init>") && !ste.getMethodName().equals("<clinit>")) {
			signature += "."+ste.getMethodName();
		}
		te.setSignature(signature);
		return te;
	}

	public SinfoLog getBasicLog() {
		return basicLog;
	}

	public void setBasicLog(SinfoLog basicLog) {
		this.basicLog = basicLog;
	}

	public String getIdSubsistema() {
		return idSubsistema;
	}

	public void setIdSubsistema(String idSubsistema) {
		this.idSubsistema = idSubsistema;
	}

	public String getExceptionMessage() {
		return exceptionMessage;
	}

	public void setExceptionMessage(String exceptionMessage) {
		this.exceptionMessage = exceptionMessage;
	}

	public String getClasseExcecao() {
		return classeExcecao;
	}

	public void setClasseExcecao(String classeExcecao) {
		this.classeExcecao = classeExcecao;
	}

	public Long getCodigoProcessador() {
		return codigoProcessador;
	}

	public void setCodigoProcessador(Long codigoProcessador) {
		this.codigoProcessador = codigoProcessador;
	}

	public String getLoginUsuario() {
		return loginUsuario;
	}

	public void setLoginUsuario(String loginUsuario) {
		this.loginUsuario = loginUsuario;
	}

	public String getRootCause() {
		return rootCause;
	}

	public void setRootCause(String rootCause) {
		this.rootCause = rootCause;
	}

	public List<StackTraceElement> getStackTraceElements() {
		return stackTraceElements;
	}

	public void setStackTraceElements(List<StackTraceElement> stackTraceElements) {
		this.stackTraceElements = stackTraceElements;
	}

	public void setTraceElements(List<TraceElement> traceElements) {
		this.traceElements = traceElements;
	}
}
