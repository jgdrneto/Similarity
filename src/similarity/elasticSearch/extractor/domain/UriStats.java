package similarity.elasticSearch.extractor.domain;

public class UriStats implements Comparable<UriStats>{
	private String uri;
	private Long count;
	
	public UriStats(String uri, Long count) {
		this.uri = uri;
		this.count = count;
	}

	@Override
	public int compareTo(UriStats o) {
		int ret = o.getCount().compareTo(this.count);
		
		if(ret == 0) {
			ret = o.getUri().compareTo(this.uri);
		}
		
		return ret;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}
}
