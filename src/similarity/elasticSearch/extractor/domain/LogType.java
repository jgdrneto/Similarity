package similarity.elasticSearch.extractor.domain;

public enum LogType {
	ACCESS("Access"),
	ERROR("Error"),
	EXECUTION("Execution");

	private final String label;

	private LogType(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}
}
