package similarity.elasticSearch.extractor.domain;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;


public abstract class AbstractWebExcecutionTrace implements WebExecutionTrace {
	private String traceElementsString = null;
	private Integer height = null;
	private Integer qtdDescendents = null;
	protected Map<String, String> moreInformation = new TreeMap<>();
	
	@Override
	public String getRequestURI () {
		int indexStart = getRequestURL().indexOf("/", 8);//Primeira / depois do http:// ou https://
		int indexEnd = getRequestURL().indexOf("?");
		if(indexEnd < 0) {
			indexEnd = getRequestURL().length();
		}
		return getRequestURL().substring(indexStart, indexEnd);
	}
	
	@Override
	public boolean contains(TraceElement traceElement) {
		boolean contains = false;
		
		for(TraceElement te : getTraceElements()) {
			if(te.contains(traceElement)) {
				contains = true;
				break;
			}
		}
		
		return contains;
	}
	
	@Override
	public boolean contains(List<TraceElement> traceElements) {
		boolean contains = true;
		
		for(TraceElement te : traceElements) {
			if(!contains(te)) {
				contains = false;
				break;
			}
		}
		
		return contains;
	}
	
	@Override
	public boolean equivalent(WebExecutionTrace other) {
		if(this.getRequestURI().equals(other.getRequestURI()) && this.contains(other.getTraceElements()) && other.contains(this.getTraceElements())) {
			return true;
		}
		return false;
	}
	
	@Override
	public String getTraceElementsAsString() {
		if(traceElementsString == null) {
			StringBuilder sb = new StringBuilder();
			for(TraceElement te : getTraceElements()) {
				sb.append(te.toString());
			}
			traceElementsString = sb.toString().trim();
		}
		return traceElementsString;
	}
	
	@Override
	public Integer getHeight() {
		if(height == null) {
			height = 0;
			for(TraceElement te : getTraceElements()) {
				int teHeight = te.getHeight(); 
				if(teHeight > height) {
					height = teHeight;
				}
			}
		}
		return height;
	}
	
	@Override
	public Integer getQtdTraceElements() {
		if(qtdDescendents == null) {
			qtdDescendents = getTraceElements().size();
			for(TraceElement te : getTraceElements()) {
				qtdDescendents += te.getQtdDescendents(); 
			}
		}
		return qtdDescendents;
	}
}
