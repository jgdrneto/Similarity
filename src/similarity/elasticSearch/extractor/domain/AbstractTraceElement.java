package similarity.elasticSearch.extractor.domain;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractTraceElement implements TraceElement {
	public List<TraceElement> getTraceElement(String signature) {
		List<TraceElement> traceElements = new ArrayList<>();
		
		if(getSignature().equals(signature)){
			traceElements.add(this);
		}
		for(TraceElement te : getChildren()) {
			traceElements.addAll(te.getTraceElement(signature));
		}
		
		return traceElements;
	}
	
	public boolean contains(TraceElement traceElement) {
		boolean contains = true;
		
		List<TraceElement> tes = getTraceElement(traceElement.getSignature());
		if(!tes.isEmpty()){
			for(TraceElement child : traceElement.getChildren()) {
				boolean containsChild = false;
				for(TraceElement te : tes) {
					if(te.contains(child)) {
						containsChild = true;
						break;
					}
				}
				if(!containsChild) {
					contains = false;
					break;
				}
			}
		}else {
			contains = false;
		}
		
		return contains;
	}
	
	@Override
	public String toString() {
		return toString("");
	}
	
	public String toString(String prefix) {
		StringBuilder sb = new StringBuilder();
		sb.append(this.getSignature()).append("\n");
		for(TraceElement c : getChildren()) {
			String pre = prefix+"\t";
			sb.append(pre+c.toString(pre));
		}
		return sb.toString();
	}
	
	@Override
	public Integer getHeight(){
		int hChildren = 0;
		for(TraceElement c : getChildren()) {
			hChildren = Math.max(hChildren, c.getHeight());
		}
		return hChildren+1;
	}
	
	@Override
	public Integer getQtdDescendents() {
		int qtd = getChildren().size();
		for(TraceElement c : getChildren()) {
			qtd += c.getQtdDescendents();
		}
		return qtd;
	}
}
