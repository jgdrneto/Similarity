package similarity.elasticSearch.extractor.domain;

import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

public class SinfoAccessLog implements Log {
	private SinfoLog basicLog;
	private String idSubsistema;
	private String idRegistroAcessoPublico;
	private String mensagens;
	private String parametros;
	private Integer tempo;
	private Boolean erro;
	private Boolean migradoPostgresql;
	
	private Map<String, String> moreInformation = new TreeMap<>();
	
	public SinfoAccessLog(SinfoLog basicLog, String idSubsistema,
			String idRegistroAcessoPublico, String mensagens, String parametros, Integer tempo, Boolean erro,
			Boolean migradoPostgresql) {
		this.basicLog = basicLog;
		this.idSubsistema = idSubsistema;
		this.idRegistroAcessoPublico = idRegistroAcessoPublico;
		this.mensagens = mensagens;
		this.parametros = parametros;
		this.tempo = tempo;
		this.erro = erro;
		this.migradoPostgresql = migradoPostgresql;
	}

	@Override
	public LogType getLogType() {
		return LogType.ACCESS;
	}

	@Override
	public String getIdentifier() {
		return basicLog.getIdentifier();
	}

	@Override
	public String getRequestURL() {
		return basicLog.getRequestURI();
	}

	@Override
	public String getRequestURI() {
		return basicLog.getRequestURI();
	}

	@Override
	public Date getDate() {
		return basicLog.getDate();
	}
	
	@Override
	public Map<String, String> getMoreInformation() {
		moreInformation = basicLog.getMoreInformation();
		if(idSubsistema != null) {
			moreInformation.put("id_subsistema", idSubsistema);
		}
		if(idRegistroAcessoPublico != null) {
			moreInformation.put("id_registro_acesso_publico", idRegistroAcessoPublico);
		}
		if(erro != null) {
			moreInformation.put("erro", erro.toString());
		}
		if(parametros != null) {
			moreInformation.put("parametros", parametros);
		}
		if(tempo != null) {
			moreInformation.put("tempo", tempo.toString());
		}
		if(migradoPostgresql != null) {
			moreInformation.put("migrado_postgresql", String.valueOf(migradoPostgresql));
		}
		if(mensagens != null) {
			moreInformation.put("mensagens", mensagens);
		}
		return moreInformation;
	}

	public SinfoLog getBasicLog() {
		return basicLog;
	}

	public void setBasicLog(SinfoLog basicLog) {
		this.basicLog = basicLog;
	}

	public String getIdSubsistema() {
		return idSubsistema;
	}

	public void setIdSubsistema(String idSubsistema) {
		this.idSubsistema = idSubsistema;
	}

	public String getIdRegistroAcessoPublico() {
		return idRegistroAcessoPublico;
	}

	public void setIdRegistroAcessoPublico(String idRegistroAcessoPublico) {
		this.idRegistroAcessoPublico = idRegistroAcessoPublico;
	}

	public String getMensagens() {
		return mensagens;
	}

	public void setMensagens(String mensagens) {
		this.mensagens = mensagens;
	}

	public String getParametros() {
		return parametros;
	}

	public void setParametros(String parametros) {
		this.parametros = parametros;
	}

	public Integer getTempo() {
		return tempo;
	}

	public void setTempo(Integer tempo) {
		this.tempo = tempo;
	}

	public Boolean getErro() {
		return erro;
	}

	public void setErro(Boolean erro) {
		this.erro = erro;
	}

	public Boolean getMigradoPostgresql() {
		return migradoPostgresql;
	}

	public void setMigradoPostgresql(Boolean migradoPostgresql) {
		this.migradoPostgresql = migradoPostgresql;
	}

	public void setMoreInformation(Map<String, String> moreInformation) {
		this.moreInformation = moreInformation;
	}
}
