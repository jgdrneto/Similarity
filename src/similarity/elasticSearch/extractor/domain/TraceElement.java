package similarity.elasticSearch.extractor.domain;

import java.util.List;

public interface TraceElement {
	public String getSignature();
	public TraceElement getParent();
	public List<? extends TraceElement> getChildren();
	public List<TraceElement> getTraceElement(String signature);
	public boolean contains(TraceElement traceElement);
	String toString(String prefix);
	public Integer getHeight();
	public Integer getQtdDescendents();
}
