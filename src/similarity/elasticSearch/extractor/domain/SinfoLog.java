package similarity.elasticSearch.extractor.domain;

import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

public class SinfoLog {
	private String id;
	private Date date;
	private String url;
	private Integer idSistema;
	private String idRegistroEntrada;
	private String hashIdentificacao;
	private String hashLogOperacao;
	private String logNivel;
	private String category;
	private Integer count;
	private String host;
	private String port;
	private String inputType;
	private String message;
	private String source;
	private String type;
		
	private Map<String, String> moreInformation = new TreeMap<>();
	
	public SinfoLog(String id, Date date, String url, Integer idSistema, String idRegistroEntrada, String hashIdentificacao,
			String hashLogOperacao, String logNivel, String category, Integer count, String host, String port,
			String inputType, String message, String source, String type) {
		super();
		this.id = id;
		this.date = date;
		this.url = url;
		this.idSistema = idSistema;
		this.idRegistroEntrada = idRegistroEntrada;
		this.hashIdentificacao = hashIdentificacao;
		this.hashLogOperacao = hashLogOperacao;
		this.logNivel = logNivel;
		this.category = category;
		this.count = count;
		this.host = host;
		this.port = port;
		this.inputType = inputType;
		this.message = message;
		this.source = source;
		this.type = type;
	}
	
	public String getRequestURL() {
		return url;
	}
	
	public String getRequestURI () {
		int indexStart = getRequestURL().indexOf("/", 8);//Primeira / depois do http:// ou https://
		int indexEnd = getRequestURL().indexOf("?");
		if(indexEnd < 0) {
			indexEnd = getRequestURL().length();
		}
		return getRequestURL().substring(indexStart, indexEnd);
	}
	
	public String getIdentifier() {
		return id;
	}
	
	public Map<String, String> getMoreInformation() {
		if(idSistema != null ) {
			moreInformation.put("id_sistema", idSistema.toString());
		}
		if(idRegistroEntrada != null ) {
			moreInformation.put("id_registro_entrada", idRegistroEntrada);
		}
		if(hashIdentificacao != null ) {
			moreInformation.put("hash_identificacao", hashIdentificacao);
		}
		if(hashLogOperacao != null ) {
			moreInformation.put("hash_log_operacao", hashLogOperacao);
		}
		if(logNivel != null ) {
			moreInformation.put("log_nivel", logNivel);
		}
		if(category != null ) {
			moreInformation.put("category", category);
		}
		if(count != null ) {
			moreInformation.put("count", count.toString());
		}
		if(host != null ) {
			moreInformation.put("host", host);
		}
		if(port != null ) {
			moreInformation.put("port", port);
		}
		if(inputType != null ) {
			moreInformation.put("input_type", inputType);
		}
		if(message != null ) {
			moreInformation.put("source", message);
		}
		if(type != null ) {
			moreInformation.put("type", type);
		}
		return moreInformation;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Integer getIdSistema() {
		return idSistema;
	}

	public void setIdSistema(Integer idSistema) {
		this.idSistema = idSistema;
	}

	public String getIdRegistroEntrada() {
		return idRegistroEntrada;
	}

	public void setIdRegistroEntrada(String idRegistroEntrada) {
		this.idRegistroEntrada = idRegistroEntrada;
	}

	public String getHashIdentificacao() {
		return hashIdentificacao;
	}

	public void setHashIdentificacao(String hashIdentificacao) {
		this.hashIdentificacao = hashIdentificacao;
	}

	public String getHashLogOperacao() {
		return hashLogOperacao;
	}

	public void setHashLogOperacao(String hashLogOperacao) {
		this.hashLogOperacao = hashLogOperacao;
	}

	public String getLogNivel() {
		return logNivel;
	}

	public void setLogNivel(String logNivel) {
		this.logNivel = logNivel;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getInputType() {
		return inputType;
	}

	public void setInputType(String inputType) {
		this.inputType = inputType;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setMoreInformation(Map<String, String> moreInformation) {
		this.moreInformation = moreInformation;
	}
}
