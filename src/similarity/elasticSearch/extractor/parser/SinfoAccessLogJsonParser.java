package similarity.elasticSearch.extractor.parser;

import java.util.Map;

import com.fasterxml.jackson.databind.JsonNode;

import similarity.elasticSearch.extractor.domain.SinfoAccessLog;


public class SinfoAccessLogJsonParser {
	SinfoLogJsonParser sinfoLogJsonParser = new SinfoLogJsonParser();
	
	public SinfoAccessLogJsonParser() {
	}
	
	public SinfoAccessLog parse (JsonNode node) {
		JsonNode source = node.get("_source");
		String idSubsistema = source.get("id_subsistema") != null ? source.get("id_subsistema").asText() :null;
		String idRegistroAcessoPublico = source.get("id_registro_acesso_publico") != null ? source.get("id_registro_acesso_publico").asText() : null;
		String mensagens = source.get("mensagens") != null ? source.get("mensagens").asText() : null;
		String parametros = source.get("parametros") != null ? source.get("parametros").asText() : null;
		int tempo = source.get("tempo") != null ? source.get("tempo").asInt() : null;
		Boolean erro = source.get("erro") != null ? source.get("erro").asBoolean() : null;
		Boolean migradoPostgresql = source.get("migrado_postgresql") != null ? source.get("migrado_postgresql").asBoolean() : null;
		
		return new SinfoAccessLog(sinfoLogJsonParser.parse(node), idSubsistema, idRegistroAcessoPublico, mensagens, parametros, tempo, erro, migradoPostgresql);
	}
	
	public SinfoAccessLog parse (Map<String, Object> node) {
		@SuppressWarnings("unchecked")
		Map<String, Object> source = (Map<String, Object>) node.get("_source");
		String idSubsistema = source.get("id_subsistema") != null ? source.get("id_subsistema").toString() : null;
		String idRegistroAcessoPublico = source.get("id_registro_acesso_publico") != null ? source.get("id_registro_acesso_publico").toString() : null;
		String mensagens = source.get("mensagens") != null ? source.get("mensagens").toString() : null;
		String parametros = source.get("parametros") != null ? source.get("parametros").toString() : null;
		int tempo = source.get("tempo") != null ? Integer.parseInt(source.get("tempo").toString()) : null;
		Boolean erro = source.get("erro") != null ? Boolean.parseBoolean(source.get("erro").toString()) : null;
		Boolean migradoPostgresql = source.get("migrado_postgresql") != null ? Boolean.parseBoolean(source.get("migrado_postgresql").toString()) : null;
		
		return new SinfoAccessLog(sinfoLogJsonParser.parse(node), idSubsistema, idRegistroAcessoPublico, mensagens, parametros, tempo, erro, migradoPostgresql);
	}
}