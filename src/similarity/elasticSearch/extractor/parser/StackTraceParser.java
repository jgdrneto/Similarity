package similarity.elasticSearch.extractor.parser;


import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StackTraceParser {
	private final static Pattern PatternFullLog = Pattern.compile("(at\\s)(?<signature>(?<fullClassName>.+)(\\.)(?<methodName>[^\\.]*))(\\()(?<parameters>([^\\)]*))(\\))");
	
	private String regexSignature;
	private String regexIgnoreSignature;
	
	public StackTraceParser() {
		
	}
	
	public StackTraceParser(String regexSignature) {
		this.regexSignature = regexSignature;
	}
	
	public StackTraceParser(String regexSignature, String regexIgnoreSignature) {
		this.regexSignature = regexSignature;
		this.regexIgnoreSignature = regexIgnoreSignature;
	}
	
	public List<StackTraceElement> parse (String stringStackTrace){
		List<StackTraceElement> elements = new ArrayList<StackTraceElement>();
	
		String javaStackTrace = null;
		if(!stringStackTrace.contains("Stacktrace:")) {
			javaStackTrace = stringStackTrace;
		}else {
			String[] s2 = stringStackTrace.split("Stacktrace:");
			if(s2.length > 1) {
				javaStackTrace = "Stacktrace:\n"+s2[1];
			}else {
				javaStackTrace = "";
			}
		}
		
		String[] s = javaStackTrace.split("Caused by: ");
		
		for(int i=s.length-1; i >= 0; i--){
			Matcher matcher = PatternFullLog.matcher(s[i]);
			while(matcher.find()) {
				String classAndMethod = matcher.group("signature");
				if(classAndMethod != null && regexSignature == null || (regexSignature != null && classAndMethod.matches(regexSignature))){
//					log.debug(classAndMethod + "matches with regexSignature (" + regexSignature + ")");
					if(regexIgnoreSignature == null || !classAndMethod.matches(regexIgnoreSignature)){
//						log.debug(classAndMethod + "not matches with regexIgnoreSignature (" + regexIgnoreSignature + "): Including");
						String declaringClass = matcher.group("fullClassName");
						String methodName = matcher.group("methodName");
						String fileName = matcher.group("parameters");
						int lineNumber = -1;
						if(fileName.contains(":")){
							String [] parameters = fileName.split(":");
							fileName = parameters[0];
							lineNumber = Integer.parseInt(parameters[1]);
						}
						elements.add(new StackTraceElement(declaringClass, methodName, fileName, lineNumber));
//					}else {
//						log.debug(classAndMethod + " matches with regexIgnoreSignature (" + regexIgnoreSignature + "): Excluding");
					}
				}
		    }
		}
		
		return elements;
	}
}