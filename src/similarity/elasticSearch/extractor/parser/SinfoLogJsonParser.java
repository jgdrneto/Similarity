package similarity.elasticSearch.extractor.parser;

import static javax.xml.bind.DatatypeConverter.parseDateTime;

import java.util.Date;
import java.util.Map;

import com.fasterxml.jackson.databind.JsonNode;

import similarity.elasticSearch.extractor.domain.SinfoLog;



public class SinfoLogJsonParser {
	
	public SinfoLogJsonParser() {
	}
	
	public SinfoLog parse (JsonNode node) {
		String id = node.get("_id").asText();
		JsonNode src = node.get("_source");
		Date date = parseDateTime(src.get("data_hora_operacao").asText()).getTime();
		String url = src.get("url").asText();
		int idSistema = src.get("id_sistema") != null ? src.get("id_sistema").asInt() : null;
		String idRegistroEntrada = src.get("id_registro_entrada") != null ? src.get("id_registro_entrada").asText() : null;
		String hashIdentificacao = src.get("hash_identificacao") != null?src.get("hash_identificacao").asText() : null;
		String hashLogOperacao = src.get("hash_log_operacao") != null?src.get("hash_log_operacao").asText() : null;
		String logNivel = src.get("log_nivel") != null ? src.get("log_nivel").asText() : null;
		String category = src.get("category") != null ? src.get("category").asText() : null;
		Integer count = src.get("count") != null ? src.get("count").asInt() : null;
		String host = src.get("host") != null ? src.get("host").asText() : null;
		String port = src.get("port") != null ? src.get("port").asText() : null;
		String inputType = src.get("input_type") != null ? src.get("input_type").asText() : null;
		String message = src.get("message") != null ? src.get("message").asText() : null;
		String type = src.get("type") != null ? src.get("type").asText() : null;
		String source = src.get("source") != null ? src.get("source").asText() : null;
		
		return new SinfoLog(id, date, url, idSistema, idRegistroEntrada, hashIdentificacao, hashLogOperacao, logNivel, category,
				count, host, port, inputType, message, source, type);
	}
	
	public SinfoLog parse (Map<String, Object> node) {
		String id = node.get("_id").toString();
		@SuppressWarnings("unchecked")
		Map<String, Object> src = (Map<String, Object>) node.get("_source");
		Date date = parseDateTime(src.get("data_hora_operacao").toString()).getTime();
		String url = src.get("url").toString();
		int idSistema = Integer.parseInt(src.get("id_sistema").toString());
		String idRegistroEntrada = src.get("id_registro_entrada") != null ? src.get("id_registro_entrada").toString() : null;
		String hashIdentificacao = src.get("hash_identificacao") != null ? src.get("hash_identificacao").toString() : null;
		String hashLogOperacao = src.get("hash_log_operacao") != null ? src.get("hash_log_operacao").toString() : null;
		String logNivel = src.get("log_nivel") != null ? src.get("log_nivel").toString() : null;
		String category = src.get("category") != null ? src.get("category").toString() : null;
		Integer count = src.get("count") != null ? Integer.parseInt(src.get("count").toString()) : null;
		String host = src.get("host") != null ? src.get("host").toString() : null;
		String port = src.get("port") != null ? src.get("port").toString() : null;
		String inputType = src.get("input_type") != null ? src.get("input_type").toString() : null;
		String message = src.get("message") != null ? src.get("message").toString() : null;
		String type = src.get("type") != null ? src.get("type").toString() : null;
		String source = src.get("source") != null ? src.get("source").toString() : null;
		
		
		return new SinfoLog(id, date, url, idSistema, idRegistroEntrada, hashIdentificacao, hashLogOperacao, logNivel, category,
				count, host, port, inputType, message, source, type);
	}
}