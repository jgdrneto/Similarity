package similarity.elasticSearch.extractor.parser;

import java.util.Map;

import com.fasterxml.jackson.databind.JsonNode;

import similarity.elasticSearch.extractor.domain.SinfoErrorLog;


public class SinfoErrorLogJsonParser {
	SinfoLogJsonParser sinfoLogJsonParser = new SinfoLogJsonParser();
	private StackTraceParser stackTraceParser;
	
	public SinfoErrorLogJsonParser() {
		stackTraceParser = new StackTraceParser();
	}
	
	public SinfoErrorLogJsonParser(StackTraceParser stackTraceParser) {
		this.stackTraceParser = stackTraceParser;
	}
	
	public SinfoErrorLogJsonParser(String regexSignature) {
		stackTraceParser = new StackTraceParser(regexSignature);
	}
	
	public SinfoErrorLogJsonParser(String regexSignature, String regexIgnoreSignature) {
		stackTraceParser = new StackTraceParser(regexSignature, regexIgnoreSignature);
	}
	
	public SinfoErrorLog parse (JsonNode node) {
		JsonNode source = node.get("_source");
		String stackTraceLogString = source.get("stacktrace") != null ? source.get("stacktrace").asText() : null;
		String idSubsistema = source.get("id_subsistema") != null ? source.get("id_subsistema").asText() : null;
		String exceptionMessage = source.get("message") != null ? source.get("message").asText() : null;
		String classeExcecao = source.get("classe_excecao") != null ? source.get("classe_excecao").asText() : null;
		Long codigoProcessador = source.get("codigo_processador") != null ? source.get("codigo_processador").asLong() : null;
		String loginUsuario = source.get("login_usuario") != null ? source.get("login_usuario").asText() : null;
		String rootCause = source.get("root_cause") != null ? source.get("root_cause").asText():null;
		
		return new SinfoErrorLog(sinfoLogJsonParser.parse(node), idSubsistema, exceptionMessage, classeExcecao, codigoProcessador, loginUsuario,
				rootCause, stackTraceParser.parse(stackTraceLogString));
	}
	
	public SinfoErrorLog parse (Map<String, Object> node) {
		@SuppressWarnings("unchecked")
		Map<String, Object> source = (Map<String, Object>) node.get("_source");
		String stackTraceLogString = source.get("stacktrace") != null ? source.get("stacktrace").toString() : null;
		String idSubsistema = source.get("id_subsistema") != null ? source.get("id_subsistema").toString() : null;
		String exceptionMessage = source.get("message") != null ? source.get("message").toString() : null;
		String classeExcecao = source.get("classe_excecao") != null ? source.get("classe_excecao").toString() : null;
		Long codigoProcessador = source.get("codigo_processador") != null ? Long.parseLong(source.get("codigo_processador").toString()) : null;
		String loginUsuario = source.get("login_usuario") !=null ? source.get("login_usuario").toString() : null;
		String rootCause = source.get("root_cause") != null ? source.get("root_cause").toString() : null;
		
		return new SinfoErrorLog(sinfoLogJsonParser.parse(node), idSubsistema, exceptionMessage, classeExcecao, codigoProcessador, loginUsuario,
				rootCause, stackTraceParser.parse(stackTraceLogString));
	}
}