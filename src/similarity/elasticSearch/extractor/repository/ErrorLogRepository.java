/*
 * CASE The Collaborative & Automated Software Engineering Research Group 
 * Federal University of Rio Grande do Norte
 * ElasticScenarioExtractor.java 
 * 4 de nov de 2017
 */
package similarity.elasticSearch.extractor.repository;

import java.io.IOException;
import java.util.Optional;

import similarity.elasticSearch.extractor.domain.WebExecutionTrace;
import similarity.elasticSearch.extractor.parser.StackTraceParser;


/**
 * @author Jadson Santos - jadsonjs@gmail.com
 *
 */
public interface ErrorLogRepository extends WebExecutionRepository {
	Optional<WebExecutionTrace> findById(String id, StackTraceParser stackTraceParser) throws IOException;
}
