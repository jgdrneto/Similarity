/*
 * CASE The Collaborative & Automated Software Engineering Research Group 
 * Federal University of Rio Grande do Norte
 * ElasticScenarioExtractor.java 
 * 4 de nov de 2017
 */
package similarity.elasticSearch.extractor.repository;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import similarity.elasticSearch.extractor.domain.Log;
import similarity.elasticSearch.extractor.domain.UriStats;
import similarity.elasticSearch.extractor.parser.SinfoAccessLogJsonParser;



/**
 * @author Jadson Santos - jadsonjs@gmail.com
 *
 */
public class SinfoAccessLogRepository extends AbstractLogRepository implements AccessLogRepository {
	SinfoAccessLogJsonParser accessJsonParser = new SinfoAccessLogJsonParser();
	
	@Override
	String getIndexName() {
		return "indice_log_operacao-*";
	}

	@Override
	String getDocumentType() {
		return "log_operacao";
	}
	
	@Override
	public List<UriStats> countGroupedByUri(Date start, Date end) throws IOException {
		List<UriStats> ret = new ArrayList<>();
		Map<String, Long> urisCount = getUrisCount(start, end);
		for(Entry<String, Long> uc : urisCount.entrySet()) {
			ret.add(new UriStats(uc.getKey(), uc.getValue()));
		}
		return ret;
	}
	
	private List<Log> getByQuery(String query) throws IOException {
		List<Log> sinfoLogs = new ArrayList<>();
		
		String response = queryElasticSearch(query);
		ObjectMapper mapper = new ObjectMapper();
		mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSS'Z'"));
		JsonNode rootNode = mapper.readTree(response);
		JsonNode hitsNode = null;
		if(rootNode.get("responses") == null) {
			hitsNode = rootNode.get("hits").get("hits");
		}else {
			hitsNode = rootNode.get("responses").get(0).get("hits").get("hits");
		} 
		Iterator<JsonNode> i = hitsNode.elements();
		while(i.hasNext()) {
			sinfoLogs.add(accessJsonParser.parse(i.next()));
		}
		
		return sinfoLogs;	
	}

	@Override
	public Optional<Log> findById(String id) throws IOException {
		Optional<Log> ret = Optional.empty();
		String query = "{\"query\": {\"match\" : {\"_id\" : \""+id+"\"}}}";
		List<Log> list = getByQuery(query);
		if(!list.isEmpty()) {
			ret = Optional.of(list.get(0));
		}
		return ret;
	}

	@Override
	public List<String> findIdsByDateBetween(Date start, Date end) throws IOException {
		String query = "{\"size\": 10000, \"query\": {\"range\":{\"data_hora_operacao\":{\"gte\":"+start.getTime()+",\"lte\":"+end.getTime()+",\"format\":\"epoch_millis\"}}}, \"_source\": [\"_id\"]}";
		return findIds(query);
	}
	
	@Override
	public List<String> findIdsByUriAndDateBetween(String uri, Date start, Date end) throws IOException {
		String query = "{\"size\": 4000, \"query\": {\"bool\": {\"must\": {\"wildcard\" : { \"url.raw\" : \"*"+uri+"*\" }},\"filter\": {\"range\":{\"data_hora_operacao\":{\"gte\":"+start.getTime()+",\"lte\":"+end.getTime()+",\"format\":\"epoch_millis\"}}}}}, \"_source\": [\"_id\"]}";
		return findIds(query);
	}
}
