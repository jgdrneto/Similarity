package similarity.elasticSearch.extractor.repository;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;


/**
 * @author jadson santos - jadsonjs@gmail.com
 *
 */
public abstract class AbstractLogRepository {
	abstract String getIndexName();
	abstract String getDocumentType();
	
	/**
	 * To search in a specific index and type, the following convention is used:
	 * POST localhost:9200/index/type/_search
	 */
	public final static String TEMPLATE_URL_QUERY_ELASTICSEARCH = "%s/%s/%s/" + "_search?pretty";
	
    String url;
    String username;
	String password;
	
	/**
	 * Curl is a computer software project providing a library and command-line tool for transferring data using various protocols
	 * @param query query
	 * @return the CURL command
	 */
	public String gerarateCurlCommmandElasticSearch(String query) {
		return "\ncurl"+" -XGET "+"'"+getUrlConnection()+ "/" + getIndexName() + "/" + getDocumentType() + "/_search?pretty'" +" -d ' \n" + query + " '\n";
	}
	
	public String getUrlConnection() {
		if(username != null) {
			return url.replace("//", "//"+username+":"+password+"@");
		}
		return url;
	}
	
	protected String queryElasticSearch(String query) throws IOException{
		HttpClient httpClient = HttpClientBuilder.create().build();
		
		String urlRequisitadaElasticsearch = String.format(TEMPLATE_URL_QUERY_ELASTICSEARCH, getUrlConnection(), getIndexName(), getDocumentType());
		gerarateCurlCommmandElasticSearch(query);
		HttpPost httpPost = new HttpPost(urlRequisitadaElasticsearch);
		httpPost.setHeader("Accept", "application/json");
		httpPost.setHeader("Content-type", "application/json");
		
		StringEntity entityParametrosQueryJson = new StringEntity(query, ContentType.APPLICATION_JSON);
		entityParametrosQueryJson.setContentType(new BasicHeader(HTTP.CONTENT_TYPE,"application/json"));
		httpPost.setEntity(entityParametrosQueryJson);
		
		HttpResponse response = httpClient.execute(httpPost);
		if (response.getStatusLine().getStatusCode() != 200) {
			throw new RuntimeException("\nElasticsearch Error:"
					+ "\n HTTP de Error: " +	response.getStatusLine().getStatusCode()+" "+response.getStatusLine().getReasonPhrase()+" "
					+ "\n Details: \n" + response.getStatusLine().toString() );
		}
		return EntityUtils.toString(response.getEntity());
	}
	
	protected Map<String, Long> getUrisCount(Date start, Date end) throws IOException {
		Map<String, Long> counts = new LinkedHashMap<>();
		
		String query = "{\"size\":0,\"query\":{\"bool\":{\"must\":[{\"match_all\":{}},{\"range\":{\"data_hora_operacao\":{\"gte\":"+start.getTime()+",\"lte\":"+end.getTime()+",\"format\":\"epoch_millis\"}}}],\"must_not\":[]}},\"_source\":{\"excludes\":[]},\"aggs\":{\"aggs\":{\"terms\":{\"field\":\"url.raw\",\"size\":1000,\"order\":{\"_count\":\"desc\"}}}}}";
		String response = queryElasticSearch(query);
		ObjectMapper mapper = new ObjectMapper();
		mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSS'Z'"));
		JsonNode rootNode = mapper.readTree(response);
		
		JsonNode buckets = rootNode.path("aggregations").path("aggs").path("buckets");

		//TODO Verificar se é possível fazer a agregação por URI (no lugar de URL) diretamente no elasticsearch, evitando o laço abaixo 
		if(buckets.isArray() && buckets.size() > 0){
			ArrayNode arrayNode = (ArrayNode) buckets;
			Iterator<JsonNode> iterator = arrayNode.elements();
			while( iterator.hasNext() ){
				JsonNode node = iterator.next();

				String url =  node.get("key").asText();
				int index1 = Math.max(0, url.indexOf("//"));
				int index2 = Math.max(0, url.indexOf("/", index1+2));
				int index3 = url.indexOf("?");
				if(index3 < 0) {
					index3 = url.length();
				}
				String uri = url.substring(index2, index3);
				Long count = node.get("doc_count").asLong();
				if(counts.containsKey(uri)) {
					count += counts.get(uri);
				}

				counts.put(uri, count);
			}
		}
		
		return counts;
	}
	
	protected List<String> findIds(String query) throws IOException {
		List<String> list = new ArrayList<>();
		
		String response = queryElasticSearch(query);
		ObjectMapper mapper = new ObjectMapper();
		JsonNode rootNode = mapper.readTree(response);
		JsonNode hitsNode = rootNode.get("hits").get("hits"); 
		Iterator<JsonNode> i = hitsNode.elements();
		while(i.hasNext()) {
			list.add(i.next().get("_id").asText());
		}
		
		return list;
	}
	
	public List<String> findIdsByProperty(String propertyName, Object value) throws IOException {
		String query = "{\"size\": 10000, \"query\": {\"match\" : {\""+propertyName+"\" : \""+value.toString()+"\"}}, \"_source\": [\"_id\"]}";
		return findIds(query);
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public static String getTemplateUrlQueryElasticsearch() {
		return TEMPLATE_URL_QUERY_ELASTICSEARCH;
	}
}
