/*
 * CASE The Collaborative & Automated Software Engineering Research Group 
 * Federal University of Rio Grande do Norte
 * ElasticScenarioExtractor.java 
 * 4 de nov de 2017
 */
package similarity.elasticSearch.extractor.repository;

/**
 * @author Jadson Santos - jadsonjs@gmail.com
 *
 */
public interface AccessLogRepository extends LogRepository {
}
