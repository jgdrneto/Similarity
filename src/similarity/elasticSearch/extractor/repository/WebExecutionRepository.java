/*
 * CASE The Collaborative & Automated Software Engineering Research Group 
 * Federal University of Rio Grande do Norte
 * ScenarioRepository.java
 * 31 de out de 2017
 */
package similarity.elasticSearch.extractor.repository;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import similarity.elasticSearch.extractor.domain.UriStats;
import similarity.elasticSearch.extractor.domain.WebExecutionTrace;


/**
 * Represents a repository of scenarios
 * @author Jadson Santos - jadsonjs@gmail.com
 *
 */
public interface WebExecutionRepository extends LogRepository{
	Optional<WebExecutionTrace> findById(String id) throws IOException;
	List<UriStats> countGroupedByUri(Date start, Date end) throws IOException;
	List<String> findIdsByDateBetween(Date start, Date end) throws IOException;
	List<String> findIdsByUriAndDateBetween(String uri, Date start, Date end) throws IOException;
	List<String> findIdsByURIAndTraceElementSignatureAndDateBetween(String uri, String traceElementSignature, Date start, Date end) throws IOException;
	
}
