package similarity.criterions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import similarity.core.Criterion;
import similarity.core.entities.NumberExecutionTreeComparator;
import similarity.core.entities.Tree;

public class MoreExecutionCriterion implements Criterion{
	
	@Override
	public List<Tree> use(List<Tree> data, int metric) {

		List<Tree> sets = new ArrayList<Tree>(); 
		
		for(Tree t : data) {
			
			System.out.println("Analyzing ID scenario " + t.getId());
			
			if(sets.contains(t)) {
				
				int indexOldValue = sets.indexOf(t);
				Tree oldValue = sets.get(indexOldValue);
				oldValue.setNumberExecution(oldValue.getNumberExecution()+1);
							
			}else {
				sets.add(t);
			}
		
		}
		
		Collections.sort(sets,new NumberExecutionTreeComparator());
			
		if(metric >= sets.size()) {
			return sets;
		}else {
			return sets.subList(0, metric);
		}
		
	}
	
	@Override
	public String getName() {
		return "More Execution" ;
	}

}
