package similarity.criterions;

import java.util.ArrayList;
import java.util.List;

import similarity.core.Criterion;
import similarity.core.entities.Tree;

public class ExecutionTimeCriterion implements Criterion{
	
	@Override
	public List<Tree> use(List<Tree> data, int metric) {
				
		List<Tree> trees =  new ArrayList<Tree>();
		
		for(Tree t : data) {
			
			if(t.getTotalTimeExecution().intValue()>=metric) {
				trees.add(t);
			}
			
		}
		
		return trees;
	}

	@Override
	public String getName() {
		
		return "Execution Time";
	}
	
}
